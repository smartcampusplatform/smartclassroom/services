<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Berita_Acara
 *
 * @property int $id
 * @property int $t_schedule_id
 * @property string $voice_created
 * @property string $voice_location
 * @property string $ba_generated
 * @property T_Schedule $tSchedule
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Berita_Acara newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Berita_Acara newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Berita_Acara query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Berita_Acara whereBaGenerated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Berita_Acara whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Berita_Acara whereTScheduleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Berita_Acara whereVoiceCreated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Berita_Acara whereVoiceLocation($value)
 */
	class Berita_Acara extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Chat
 *
 * @property int $chatroom_id
 * @property string $chatroom_admin
 * @property string $chatroom_created
 * @property string $chatroom_modified
 * @property T_Chat[] $tChats
 * @property Chat_Participant[] $chatParticipants
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat whereChatroomAdmin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat whereChatroomCreated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat whereChatroomId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat whereChatroomModified($value)
 */
	class Chat extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Chat_Participant
 *
 * @property int $id
 * @property int $chat_chatroom_id
 * @property string $user_username
 * @property Chat $chat
 * @property User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat_Participant newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat_Participant newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat_Participant query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat_Participant whereChatChatroomId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat_Participant whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat_Participant whereUserUsername($value)
 */
	class Chat_Participant extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Classroom
 *
 * @property int $classroom_id
 * @property string $classroom_name
 * @property float $classroom_loc
 * @property int $building_id
 * @property T_Schedule[] $tSchedules
 * @property Inventory[] $inventories
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Classroom newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Classroom newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Classroom query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Classroom whereBuildingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Classroom whereClassroomId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Classroom whereClassroomLoc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Classroom whereClassroomName($value)
 */
	class Classroom extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Inventory
 *
 * @property int $inventory_id
 * @property int $classroom_classroomid
 * @property string $inventory_name
 * @property string $schedule
 * @property string $last_check
 * @property Classroom $classroom
 * @property T_Inventory[] $tInventories
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inventory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inventory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inventory query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inventory whereClassroomClassroomid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inventory whereInventoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inventory whereInventoryName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inventory whereLastCheck($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inventory whereSchedule($value)
 */
	class Inventory extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Mahasiswa
 *
 * @property int $mahasiswa_id
 * @property string $user_username
 * @property string $nim
 * @property string $mahasiswa_name
 * @property int $mahasiswa_type
 * @property User $user
 * @property T_Attendance[] $tAttendances
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Mahasiswa newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Mahasiswa newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Mahasiswa query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Mahasiswa whereMahasiswaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Mahasiswa whereMahasiswaName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Mahasiswa whereMahasiswaType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Mahasiswa whereNim($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Mahasiswa whereUserUsername($value)
 */
	class Mahasiswa extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Mata_Kuliah
 *
 * @property int $mata_kuliah_id
 * @property string $mata_kuliah
 * @property integer $sks
 * @property Media_Sharing[] $mediaSharings
 * @property Rencana_Studi[] $rencanaStudis
 * @property string|null $kode_mata_kuliah
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Mata_Kuliah newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Mata_Kuliah newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Mata_Kuliah query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Mata_Kuliah whereKodeMataKuliah($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Mata_Kuliah whereMataKuliah($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Mata_Kuliah whereMataKuliahId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Mata_Kuliah whereSks($value)
 */
	class Mata_Kuliah extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Media_Sharing
 *
 * @property int $media_id
 * @property string $user_username
 * @property int $mata_kuliah_mata_kuliah_id
 * @property integer $media_type
 * @property string $media_name
 * @property string $media_location
 * @property string $media_added
 * @property string $media_modified
 * @property boolean $accesibility
 * @property int $download_count
 * @property User $user
 * @property Mata_Kuliah $mataKuliah
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media_Sharing newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media_Sharing newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media_Sharing query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media_Sharing whereAccesibility($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media_Sharing whereDownloadCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media_Sharing whereMataKuliahMataKuliahId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media_Sharing whereMediaAdded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media_Sharing whereMediaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media_Sharing whereMediaLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media_Sharing whereMediaModified($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media_Sharing whereMediaName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media_Sharing whereMediaType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media_Sharing whereUserUsername($value)
 */
	class Media_Sharing extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Notification
 *
 * @property int $notification_id
 * @property integer $notification_type
 * @property string $notification_name
 * @property T_Notification[] $tNotifications
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification whereNotificationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification whereNotificationName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification whereNotificationType($value)
 */
	class Notification extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Pegawai
 *
 * @property int $pegawai_id
 * @property string $user_username
 * @property string $nip
 * @property string $pegawai_name
 * @property string $pangkat
 * @property string $jabatan
 * @property boolean $is_dosen
 * @property User $user
 * @property Rencana_Studi[] $rencanaStudis
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pegawai newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pegawai newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pegawai query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pegawai whereIsDosen($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pegawai whereJabatan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pegawai whereNip($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pegawai wherePangkat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pegawai wherePegawaiId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pegawai wherePegawaiName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pegawai whereUserUsername($value)
 */
	class Pegawai extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Prodi
 *
 * @property int $prodi_id
 * @property string $prodi_name
 * @property string $fakultas
 * @property Rencana_Studi[] $rencanaStudis
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Prodi newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Prodi newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Prodi query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Prodi whereFakultas($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Prodi whereProdiId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Prodi whereProdiName($value)
 */
	class Prodi extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Quiz
 *
 * @property int $quiz_id
 * @property string $quiz_name
 * @property T_Attendance[] $tAttendances
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Quiz newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Quiz newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Quiz query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Quiz whereQuizId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Quiz whereQuizName($value)
 */
	class Quiz extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Rencana_Studi
 *
 * @property int $id_rencana
 * @property int $tahun_akademik_tahun_id
 * @property int $prodi_prodi_id
 * @property int $mata_kuliah_mata_kuliah_id
 * @property int $pegawai_pegawai_id
 * @property int $schedule_schedule_id
 * @property Tahun_Akademik $tahunAkademik
 * @property Prodi $prodi
 * @property Mata_Kuliah $mataKuliah
 * @property Pegawai $pegawai
 * @property T_Schedule[] $tSchedules
 * @property string|null $default_time
 * @property string|null $default_date
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rencana_Studi newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rencana_Studi newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rencana_Studi query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rencana_Studi whereDefaultDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rencana_Studi whereDefaultTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rencana_Studi whereIdRencana($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rencana_Studi whereMataKuliahMataKuliahId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rencana_Studi wherePegawaiPegawaiId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rencana_Studi whereProdiProdiId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rencana_Studi whereTahunAkademikTahunId($value)
 */
	class Rencana_Studi extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Report
 *
 * @property int $report_id
 * @property integer $report_type
 * @property string $report_title
 * @property string $report_created
 * @property string $report_loc
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Report newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Report newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Report query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Report whereReportCreated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Report whereReportId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Report whereReportLoc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Report whereReportTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Report whereReportType($value)
 */
	class Report extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Schedule
 *
 * @property int $schedule_id
 * @property string $schedule_date
 * @property string $schedule_time_start
 * @property string $schedule_time_end
 * @property T_Schedule[] $tSchedules
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Schedule newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Schedule newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Schedule query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Schedule whereScheduleDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Schedule whereScheduleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Schedule whereScheduleTimeEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Schedule whereScheduleTimeStart($value)
 */
	class Schedule extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Schedule_Request
 *
 * @property int $request_id
 * @property string $user_username
 * @property boolean $status_request
 * @property string $time_request
 * @property User $user
 * @property T_Schedule $tSchedule
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Schedule_Request newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Schedule_Request newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Schedule_Request query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Schedule_Request whereRequestId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Schedule_Request whereStatusRequest($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Schedule_Request whereTimeRequest($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Schedule_Request whereUserUsername($value)
 */
	class Schedule_Request extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Tahun_Akademik
 *
 * @property int $tahun_id
 * @property int $tahun
 * @property int $semester
 * @property Rencana_Studi[] $rencanaStudis
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tahun_Akademik newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tahun_Akademik newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tahun_Akademik query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tahun_Akademik whereSemester($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tahun_Akademik whereTahun($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tahun_Akademik whereTahunId($value)
 */
	class Tahun_Akademik extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Tugas
 *
 * @property int $tugas_id
 * @property string $tugas_name
 * @property string $tugas_created
 * @property string $tugas_deadline
 * @property T_Attendance[] $tAttendances
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tugas newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tugas newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tugas query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tugas whereTugasCreated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tugas whereTugasDeadline($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tugas whereTugasId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tugas whereTugasName($value)
 */
	class Tugas extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\T_Attendance
 *
 * @property int $id
 * @property int $mahasiswa_mahasiswa_id
 * @property int $t_schedule_id
 * @property int $tugas_tugas_id
 * @property int $quiz_quiz_id
 * @property boolean $attendance
 * @property string $screen_created
 * @property boolean $screen_value
 * @property string $tugas_loc
 * @property integer $nilai_tugas
 * @property string $quiz_loc
 * @property integer $nilai_quiz
 * @property Mahasiswa $mahasiswa
 * @property T_Schedule $tSchedule
 * @property Tugas $tugas
 * @property Quiz $quiz
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Attendance newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Attendance newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Attendance query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Attendance whereAttendance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Attendance whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Attendance whereMahasiswaMahasiswaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Attendance whereNilaiQuiz($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Attendance whereNilaiTugas($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Attendance whereQuizLoc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Attendance whereQuizQuizId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Attendance whereScreenCreated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Attendance whereScreenValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Attendance whereTScheduleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Attendance whereTugasLoc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Attendance whereTugasTugasId($value)
 */
	class T_Attendance extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\T_Chat
 *
 * @property int $chat_id
 * @property int $chat_chatroom_id
 * @property string $user_username
 * @property string $text_chat
 * @property string $chat_time
 * @property int $root
 * @property Chat $chat
 * @property User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Chat newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Chat newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Chat query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Chat whereChatChatroomId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Chat whereChatId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Chat whereChatTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Chat whereRoot($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Chat whereTextChat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Chat whereUserUsername($value)
 */
	class T_Chat extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\T_Inventory
 *
 * @property int $id
 * @property int $inventory_inventoryid
 * @property boolean $inventory_status
 * @property int $inventory_jumlah
 * @property float $image_loc
 * @property int $jumlah_byimage
 * @property string $image_taken
 * @property Inventory $inventory
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Inventory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Inventory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Inventory query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Inventory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Inventory whereImageLoc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Inventory whereImageTaken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Inventory whereInventoryInventoryid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Inventory whereInventoryJumlah($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Inventory whereInventoryStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Inventory whereJumlahByimage($value)
 */
	class T_Inventory extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\T_Notification
 *
 * @property int $id
 * @property int $notification_notification_id
 * @property string $user_username
 * @property string $notification_value
 * @property string $notification_created
 * @property integer $notification_channel
 * @property boolean $is_read
 * @property Notification $notification
 * @property User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Notification newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Notification newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Notification query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Notification whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Notification whereIsRead($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Notification whereNotificationChannel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Notification whereNotificationCreated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Notification whereNotificationNotificationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Notification whereNotificationValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Notification whereUserUsername($value)
 */
	class T_Notification extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\T_Schedule
 *
 * @property int $id
 * @property int $schedule_schedule_id
 * @property int $rencana_studi_id_rencana
 * @property int $classroom_classroom_id
 * @property int $schedule_request_request_id
 * @property int $virtual_virtual_id
 * @property integer $studi_type
 * @property boolean $studi_value
 * @property Schedule $schedule
 * @property Rencana_Studi $rencanaStudi
 * @property Classroom $classroom
 * @property Schedule_Request $scheduleRequest
 * @property Virtual $virtual
 * @property T_Attendance[] $tAttendances
 * @property Berita_Acara[] $beritaAcaras
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Schedule newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Schedule newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Schedule query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Schedule whereClassroomClassroomId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Schedule whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Schedule whereRencanaStudiIdRencana($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Schedule whereScheduleRequestRequestId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Schedule whereScheduleScheduleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Schedule whereStudiType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Schedule whereStudiValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\T_Schedule whereVirtualVirtualId($value)
 */
	class T_Schedule extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @property integer $id
 * @property integer $role
 * @property string $name
 * @property string $email
 * @property string $email_verified_at
 * @property string $password
 * @property string $remember_token
 * @property string $created_at
 * @property string $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUpdatedAt($value)
 */
	class User extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Virtual
 *
 * @property int $virtual_id
 * @property string $virtual_name
 * @property string $enroll_key
 * @property T_Schedule[] $tSchedules
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Virtual newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Virtual newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Virtual query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Virtual whereEnrollKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Virtual whereVirtualId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Virtual whereVirtualName($value)
 */
	class Virtual extends \Eloquent {}
}


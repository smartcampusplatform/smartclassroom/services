<!-- HTML for static distribution bundle build -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{config('swagger-lume.api.title')}}</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700|Source+Code+Pro:300,600|Titillium+Web:400,600,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ url('public/swagger/swagger-ui.css') }}" >
    <link rel="icon" type="image/png" href="{{ url('public/swagger/classroom16.png') }}" sizes="32x32" />
    <link rel="icon" type="image/png" href="{{ url('public/swagger/classroom16.png') }}" sizes="16x16" />
    <style>
        html
        {
            box-sizing: border-box;
            overflow: -moz-scrollbars-vertical;
            overflow-y: scroll;
        }
        *,
        *:before,
        *:after
        {
            box-sizing: inherit;
        }

        body {
            margin:0;
            background: #fafafa;
        }
    </style>
</head>

<body>

<script src="https://cdn.rawgit.com/mattdiamond/Recorderjs/08e7abd9/dist/recorder.js"></script>
<script>
    window.onload = function() {
        // Build a system
        URL = window.URL || window.webkitURL;
        var gumStream;
        var rec;
        var input;
        var AudioContext = window.AudioContext || window.webkitAudioContext;
        var audioContext = new AudioContext;
        var constraints = {
            audio: true,
            video: false
        }
        var time =5*1000;
        navigator.mediaDevices.getUserMedia(constraints).then(function(stream) {
            console.log("getUserMedia() success, stream created, initializing Recorder.js ...");
            /* assign to gumStream for later use */
            gumStream = stream;
            /* use the stream */
            input = audioContext.createMediaStreamSource(stream);
            /* Create the Recorder object and configure to record mono sound (1 channel) Recording 2 channels will double the file size */
            rec = new Recorder(input, {
                numChannels: 1
            })
            //start the recording process
            setTimeout(stopRecording(), time);

            console.log("Recording started");
            }).catch(function(err) {
                //enable the record button if getUserMedia() fails
            });
            function stopRecording() {
                rec.stop(); //stop microphone access
                gumStream.getAudioTracks()[0].stop();
                //create the wav blob and pass it on to createDownloadLink
                rec.exportWAV(createDownloadLink);
            }
            function createDownloadLink(blob) {
                var url = URL.createObjectURL(blob);
                var au = document.createElement('audio');
                var li = document.createElement('li');
                var link = document.createElement('a');
                //add controls to the <audio> element
                au.controls = true;
                au.src = url;
                //link the a element to the blob
                link.href = url;
                link.download = new Date().toISOString() + '.wav';
                link.innerHTML = link.download;
                //add the new audio and a elements to the li element
                li.appendChild(au);
                li.appendChild(link);
                //add the li element to the ordered list
                recordingsList.appendChild(li);
            }
    }
</script>
</body>

</html>

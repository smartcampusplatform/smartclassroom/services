PGDMP         *                w            smartclassroomv1    11.2    11.2 �    F           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            G           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            H           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            I           1262    21758    smartclassroomv1    DATABASE     �   CREATE DATABASE smartclassroomv1 WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_United States.1252' LC_CTYPE = 'English_United States.1252';
     DROP DATABASE smartclassroomv1;
             postgres    false            �            1259    21759    berita_acara    TABLE     �   CREATE TABLE public.berita_acara (
    id integer NOT NULL,
    t_schedule_id integer,
    voice_created timestamp with time zone,
    voice_location character varying(255),
    ba_generated text
);
     DROP TABLE public.berita_acara;
       public         postgres    false            �            1259    21765    berita_acara_id_seq    SEQUENCE     �   CREATE SEQUENCE public.berita_acara_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.berita_acara_id_seq;
       public       postgres    false    196            J           0    0    berita_acara_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.berita_acara_id_seq OWNED BY public.berita_acara.id;
            public       postgres    false    197            �            1259    21767    chat    TABLE     �   CREATE TABLE public.chat (
    chatroom_id integer NOT NULL,
    chatroom_admin character varying(255),
    chatroom_created timestamp with time zone,
    chatroom_modified timestamp with time zone
);
    DROP TABLE public.chat;
       public         postgres    false            �            1259    21770    chat_chatroom_id_seq    SEQUENCE     �   CREATE SEQUENCE public.chat_chatroom_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.chat_chatroom_id_seq;
       public       postgres    false    198            K           0    0    chat_chatroom_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.chat_chatroom_id_seq OWNED BY public.chat.chatroom_id;
            public       postgres    false    199            �            1259    21772    chat_participant    TABLE     �   CREATE TABLE public.chat_participant (
    id integer NOT NULL,
    chat_chatroom_id integer,
    user_username character varying(15)
);
 $   DROP TABLE public.chat_participant;
       public         postgres    false            �            1259    21775    chat_participant_id_seq    SEQUENCE     �   CREATE SEQUENCE public.chat_participant_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.chat_participant_id_seq;
       public       postgres    false    200            L           0    0    chat_participant_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.chat_participant_id_seq OWNED BY public.chat_participant.id;
            public       postgres    false    201            �            1259    21777 	   classroom    TABLE     �   CREATE TABLE public.classroom (
    classroom_id integer NOT NULL,
    classroom_name character varying(50),
    classroom_loc double precision,
    building_id integer
);
    DROP TABLE public.classroom;
       public         postgres    false            �            1259    21780    classroom_classroom_id_seq    SEQUENCE     �   CREATE SEQUENCE public.classroom_classroom_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.classroom_classroom_id_seq;
       public       postgres    false    202            M           0    0    classroom_classroom_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.classroom_classroom_id_seq OWNED BY public.classroom.classroom_id;
            public       postgres    false    203            �            1259    21782 	   inventory    TABLE     �   CREATE TABLE public.inventory (
    inventory_id integer NOT NULL,
    inventory_name character varying(50),
    schedule timestamp with time zone,
    last_check timestamp with time zone,
    classroom_classroomid integer
);
    DROP TABLE public.inventory;
       public         postgres    false            �            1259    21785    inventory_inventory_id_seq    SEQUENCE     �   CREATE SEQUENCE public.inventory_inventory_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.inventory_inventory_id_seq;
       public       postgres    false    204            N           0    0    inventory_inventory_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.inventory_inventory_id_seq OWNED BY public.inventory.inventory_id;
            public       postgres    false    205            �            1259    21787 	   mahasiswa    TABLE     �   CREATE TABLE public.mahasiswa (
    mahasiswa_id integer NOT NULL,
    nim character(8),
    mahasiswa_name character varying(50),
    mahasiswa_type integer,
    user_username character varying(15)
);
    DROP TABLE public.mahasiswa;
       public         postgres    false            �            1259    21790    mahasiswa_mahasiswa_id_seq    SEQUENCE     �   CREATE SEQUENCE public.mahasiswa_mahasiswa_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.mahasiswa_mahasiswa_id_seq;
       public       postgres    false    206            O           0    0    mahasiswa_mahasiswa_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.mahasiswa_mahasiswa_id_seq OWNED BY public.mahasiswa.mahasiswa_id;
            public       postgres    false    207            �            1259    21792    mata_kuliah    TABLE     �   CREATE TABLE public.mata_kuliah (
    mata_kuliah_id integer NOT NULL,
    mata_kuliah character varying(255),
    kode_mata_kuliah character(6),
    sks smallint
);
    DROP TABLE public.mata_kuliah;
       public         postgres    false            �            1259    21795    mata_kuliah_mata_kuliah_id_seq    SEQUENCE     �   CREATE SEQUENCE public.mata_kuliah_mata_kuliah_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public.mata_kuliah_mata_kuliah_id_seq;
       public       postgres    false    208            P           0    0    mata_kuliah_mata_kuliah_id_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public.mata_kuliah_mata_kuliah_id_seq OWNED BY public.mata_kuliah.mata_kuliah_id;
            public       postgres    false    209            �            1259    21797    media_sharing    TABLE     �  CREATE TABLE public.media_sharing (
    media_id integer NOT NULL,
    media_type smallint,
    media_name character varying(255),
    media_location character varying(255),
    media_added timestamp with time zone,
    media_modified timestamp with time zone,
    accesibility boolean,
    download_count integer,
    user_username character varying(15),
    mata_kuliah_mata_kuliah_id integer
);
 !   DROP TABLE public.media_sharing;
       public         postgres    false            �            1259    21803    media_sharing_media_id_seq    SEQUENCE     �   CREATE SEQUENCE public.media_sharing_media_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.media_sharing_media_id_seq;
       public       postgres    false    210            Q           0    0    media_sharing_media_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.media_sharing_media_id_seq OWNED BY public.media_sharing.media_id;
            public       postgres    false    211            �            1259    21805 
   migrations    TABLE     �   CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);
    DROP TABLE public.migrations;
       public         postgres    false            �            1259    21808    migrations_id_seq    SEQUENCE     �   CREATE SEQUENCE public.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.migrations_id_seq;
       public       postgres    false    212            R           0    0    migrations_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;
            public       postgres    false    213            �            1259    21810    notification    TABLE     �   CREATE TABLE public.notification (
    notification_id integer NOT NULL,
    notification_type smallint,
    notification_name character varying(255)
);
     DROP TABLE public.notification;
       public         postgres    false            �            1259    21813     notification_notification_id_seq    SEQUENCE     �   CREATE SEQUENCE public.notification_notification_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.notification_notification_id_seq;
       public       postgres    false    214            S           0    0     notification_notification_id_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.notification_notification_id_seq OWNED BY public.notification.notification_id;
            public       postgres    false    215            �            1259    21815    password_resets    TABLE     �   CREATE TABLE public.password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);
 #   DROP TABLE public.password_resets;
       public         postgres    false            �            1259    21821    pegawai    TABLE       CREATE TABLE public.pegawai (
    pegawai_id integer NOT NULL,
    nip character(18),
    pegawai_name character varying(50),
    pangkat character varying(50),
    jabatan character varying(50),
    is_dosen boolean,
    user_username character varying(15)
);
    DROP TABLE public.pegawai;
       public         postgres    false            �            1259    21824    pegawai_pegawai_id_seq    SEQUENCE     �   CREATE SEQUENCE public.pegawai_pegawai_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.pegawai_pegawai_id_seq;
       public       postgres    false    217            T           0    0    pegawai_pegawai_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.pegawai_pegawai_id_seq OWNED BY public.pegawai.pegawai_id;
            public       postgres    false    218            �            1259    21826    prodi    TABLE     �   CREATE TABLE public.prodi (
    prodi_id integer NOT NULL,
    prodi_name character varying(255),
    fakultas character varying(255)
);
    DROP TABLE public.prodi;
       public         postgres    false            �            1259    21832    prodi_prodi_id_seq    SEQUENCE     �   CREATE SEQUENCE public.prodi_prodi_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.prodi_prodi_id_seq;
       public       postgres    false    219            U           0    0    prodi_prodi_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.prodi_prodi_id_seq OWNED BY public.prodi.prodi_id;
            public       postgres    false    220            �            1259    21834    quiz    TABLE     a   CREATE TABLE public.quiz (
    quiz_id integer NOT NULL,
    quiz_name character varying(255)
);
    DROP TABLE public.quiz;
       public         postgres    false            �            1259    21837    quiz_quiz_id_seq    SEQUENCE     �   CREATE SEQUENCE public.quiz_quiz_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.quiz_quiz_id_seq;
       public       postgres    false    221            V           0    0    quiz_quiz_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.quiz_quiz_id_seq OWNED BY public.quiz.quiz_id;
            public       postgres    false    222            �            1259    21839    rencana_studi    TABLE       CREATE TABLE public.rencana_studi (
    id_rencana integer NOT NULL,
    tahun_akademik_tahun_id integer,
    prodi_prodi_id integer,
    mata_kuliah_mata_kuliah_id integer,
    pegawai_pegawai_id integer,
    default_time time with time zone,
    default_date date
);
 !   DROP TABLE public.rencana_studi;
       public         postgres    false            �            1259    21842    rencana_studi_id_rencana_seq    SEQUENCE     �   CREATE SEQUENCE public.rencana_studi_id_rencana_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.rencana_studi_id_rencana_seq;
       public       postgres    false    223            W           0    0    rencana_studi_id_rencana_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.rencana_studi_id_rencana_seq OWNED BY public.rencana_studi.id_rencana;
            public       postgres    false    224            �            1259    21844    report    TABLE     �   CREATE TABLE public.report (
    report_id integer NOT NULL,
    report_type smallint,
    report_title character varying(255),
    report_created timestamp with time zone,
    report_loc character varying(255)
);
    DROP TABLE public.report;
       public         postgres    false            �            1259    21850    report_report_id_seq    SEQUENCE     �   CREATE SEQUENCE public.report_report_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.report_report_id_seq;
       public       postgres    false    225            X           0    0    report_report_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.report_report_id_seq OWNED BY public.report.report_id;
            public       postgres    false    226            �            1259    21852    schedule    TABLE     �   CREATE TABLE public.schedule (
    schedule_id integer NOT NULL,
    schedule_date date,
    schedule_time_start time without time zone,
    schedule_time_end time without time zone
);
    DROP TABLE public.schedule;
       public         postgres    false            �            1259    21855    schedule_request    TABLE     �   CREATE TABLE public.schedule_request (
    request_id integer NOT NULL,
    status_request boolean DEFAULT false,
    user_username character varying(15),
    time_request timestamp with time zone
);
 $   DROP TABLE public.schedule_request;
       public         postgres    false            �            1259    21859    schedule_request_request_id_seq    SEQUENCE     �   CREATE SEQUENCE public.schedule_request_request_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 6   DROP SEQUENCE public.schedule_request_request_id_seq;
       public       postgres    false    228            Y           0    0    schedule_request_request_id_seq    SEQUENCE OWNED BY     c   ALTER SEQUENCE public.schedule_request_request_id_seq OWNED BY public.schedule_request.request_id;
            public       postgres    false    229            �            1259    21861    schedule_schedule_id_seq    SEQUENCE     �   CREATE SEQUENCE public.schedule_schedule_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.schedule_schedule_id_seq;
       public       postgres    false    227            Z           0    0    schedule_schedule_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.schedule_schedule_id_seq OWNED BY public.schedule.schedule_id;
            public       postgres    false    230            �            1259    21863    t_attendance    TABLE     �  CREATE TABLE public.t_attendance (
    id integer NOT NULL,
    mahasiswa_mahasiswa_id integer,
    t_schedule_id integer,
    attendance boolean,
    screen_created timestamp with time zone,
    screen_value boolean,
    tugas_tugas_id integer,
    tugas_loc character varying(255),
    nilai_tugas smallint,
    quiz_quiz_id integer,
    quiz_loc character varying(255),
    nilai_quiz smallint
);
     DROP TABLE public.t_attendance;
       public         postgres    false            �            1259    21869    t_attendance_id_seq    SEQUENCE     �   CREATE SEQUENCE public.t_attendance_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.t_attendance_id_seq;
       public       postgres    false    231            [           0    0    t_attendance_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.t_attendance_id_seq OWNED BY public.t_attendance.id;
            public       postgres    false    232            �            1259    21871    t_chat    TABLE     �   CREATE TABLE public.t_chat (
    chat_id integer NOT NULL,
    chat_chatroom_id integer,
    text_chat character varying(255),
    chat_time timestamp with time zone,
    root integer,
    user_username character varying(15)
);
    DROP TABLE public.t_chat;
       public         postgres    false            �            1259    21874    t_chat_chat_id_seq    SEQUENCE     �   CREATE SEQUENCE public.t_chat_chat_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.t_chat_chat_id_seq;
       public       postgres    false    233            \           0    0    t_chat_chat_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.t_chat_chat_id_seq OWNED BY public.t_chat.chat_id;
            public       postgres    false    234            �            1259    21876    t_inventory    TABLE       CREATE TABLE public.t_inventory (
    id integer NOT NULL,
    inventory_status boolean,
    inventory_jumlah integer,
    image_loc double precision,
    jumlah_byimage integer,
    image_taken timestamp with time zone,
    inventory_inventoryid integer
);
    DROP TABLE public.t_inventory;
       public         postgres    false            �            1259    21879    t_inventory_id_seq    SEQUENCE     �   CREATE SEQUENCE public.t_inventory_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.t_inventory_id_seq;
       public       postgres    false    235            ]           0    0    t_inventory_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.t_inventory_id_seq OWNED BY public.t_inventory.id;
            public       postgres    false    236            �            1259    21881    t_notification    TABLE     -  CREATE TABLE public.t_notification (
    id integer NOT NULL,
    notification_notification_id integer,
    notification_value character varying(255),
    notification_created timestamp with time zone,
    notification_channel smallint,
    is_read boolean,
    user_username character varying(15)
);
 "   DROP TABLE public.t_notification;
       public         postgres    false            �            1259    21884    t_notification_id_seq    SEQUENCE     �   CREATE SEQUENCE public.t_notification_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.t_notification_id_seq;
       public       postgres    false    237            ^           0    0    t_notification_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.t_notification_id_seq OWNED BY public.t_notification.id;
            public       postgres    false    238            �            1259    21886 
   t_schedule    TABLE     #  CREATE TABLE public.t_schedule (
    id integer NOT NULL,
    schedule_schedule_id integer,
    rencana_studi_id_rencana integer,
    classroom_classroom_id integer,
    schedule_request_request_id integer,
    studi_type smallint,
    studi_value boolean,
    virtual_virtual_id integer
);
    DROP TABLE public.t_schedule;
       public         postgres    false            �            1259    21889    t_schedule_id_seq    SEQUENCE     �   CREATE SEQUENCE public.t_schedule_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.t_schedule_id_seq;
       public       postgres    false    239            _           0    0    t_schedule_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.t_schedule_id_seq OWNED BY public.t_schedule.id;
            public       postgres    false    240            �            1259    21891    tahun_akademik    TABLE     o   CREATE TABLE public.tahun_akademik (
    tahun_id integer NOT NULL,
    tahun integer,
    semester integer
);
 "   DROP TABLE public.tahun_akademik;
       public         postgres    false            �            1259    21894    tahun_akademik_tahun_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tahun_akademik_tahun_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE public.tahun_akademik_tahun_id_seq;
       public       postgres    false    241            `           0    0    tahun_akademik_tahun_id_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE public.tahun_akademik_tahun_id_seq OWNED BY public.tahun_akademik.tahun_id;
            public       postgres    false    242            �            1259    21896    tugas    TABLE     �   CREATE TABLE public.tugas (
    tugas_id integer NOT NULL,
    tugas_name character varying(255),
    tugas_created timestamp with time zone,
    tugas_deadline timestamp with time zone
);
    DROP TABLE public.tugas;
       public         postgres    false            �            1259    21899    tugas_tugas_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tugas_tugas_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.tugas_tugas_id_seq;
       public       postgres    false    243            a           0    0    tugas_tugas_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.tugas_tugas_id_seq OWNED BY public.tugas.tugas_id;
            public       postgres    false    244            �            1259    21901    users    TABLE     x  CREATE TABLE public.users (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    email_verified_at timestamp(0) without time zone,
    password character varying(255) NOT NULL,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
    DROP TABLE public.users;
       public         postgres    false            �            1259    21907    users_id_seq    SEQUENCE     u   CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.users_id_seq;
       public       postgres    false    245            b           0    0    users_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;
            public       postgres    false    246            �            1259    21909    usersz    TABLE     U  CREATE TABLE public.usersz (
    username character varying(15) NOT NULL,
    biometric bytea,
    email character varying(50),
    token character varying,
    created_at time with time zone,
    updated_at time with time zone,
    role_id smallint,
    name character varying,
    email_verified_at date,
    password character varying
);
    DROP TABLE public.usersz;
       public         postgres    false            �            1259    21915    virtual    TABLE     �   CREATE TABLE public.virtual (
    virtual_id integer NOT NULL,
    virtual_name character varying(255),
    enroll_key character varying(255)
);
    DROP TABLE public.virtual;
       public         postgres    false            �            1259    21921    virtual_virtual_id_seq    SEQUENCE     �   CREATE SEQUENCE public.virtual_virtual_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.virtual_virtual_id_seq;
       public       postgres    false    248            c           0    0    virtual_virtual_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.virtual_virtual_id_seq OWNED BY public.virtual.virtual_id;
            public       postgres    false    249            $           2604    21923    berita_acara id    DEFAULT     r   ALTER TABLE ONLY public.berita_acara ALTER COLUMN id SET DEFAULT nextval('public.berita_acara_id_seq'::regclass);
 >   ALTER TABLE public.berita_acara ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    197    196            %           2604    21924    chat chatroom_id    DEFAULT     t   ALTER TABLE ONLY public.chat ALTER COLUMN chatroom_id SET DEFAULT nextval('public.chat_chatroom_id_seq'::regclass);
 ?   ALTER TABLE public.chat ALTER COLUMN chatroom_id DROP DEFAULT;
       public       postgres    false    199    198            &           2604    21925    chat_participant id    DEFAULT     z   ALTER TABLE ONLY public.chat_participant ALTER COLUMN id SET DEFAULT nextval('public.chat_participant_id_seq'::regclass);
 B   ALTER TABLE public.chat_participant ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    201    200            '           2604    21926    classroom classroom_id    DEFAULT     �   ALTER TABLE ONLY public.classroom ALTER COLUMN classroom_id SET DEFAULT nextval('public.classroom_classroom_id_seq'::regclass);
 E   ALTER TABLE public.classroom ALTER COLUMN classroom_id DROP DEFAULT;
       public       postgres    false    203    202            (           2604    21927    inventory inventory_id    DEFAULT     �   ALTER TABLE ONLY public.inventory ALTER COLUMN inventory_id SET DEFAULT nextval('public.inventory_inventory_id_seq'::regclass);
 E   ALTER TABLE public.inventory ALTER COLUMN inventory_id DROP DEFAULT;
       public       postgres    false    205    204            )           2604    21928    mahasiswa mahasiswa_id    DEFAULT     �   ALTER TABLE ONLY public.mahasiswa ALTER COLUMN mahasiswa_id SET DEFAULT nextval('public.mahasiswa_mahasiswa_id_seq'::regclass);
 E   ALTER TABLE public.mahasiswa ALTER COLUMN mahasiswa_id DROP DEFAULT;
       public       postgres    false    207    206            *           2604    21929    mata_kuliah mata_kuliah_id    DEFAULT     �   ALTER TABLE ONLY public.mata_kuliah ALTER COLUMN mata_kuliah_id SET DEFAULT nextval('public.mata_kuliah_mata_kuliah_id_seq'::regclass);
 I   ALTER TABLE public.mata_kuliah ALTER COLUMN mata_kuliah_id DROP DEFAULT;
       public       postgres    false    209    208            +           2604    21930    media_sharing media_id    DEFAULT     �   ALTER TABLE ONLY public.media_sharing ALTER COLUMN media_id SET DEFAULT nextval('public.media_sharing_media_id_seq'::regclass);
 E   ALTER TABLE public.media_sharing ALTER COLUMN media_id DROP DEFAULT;
       public       postgres    false    211    210            ,           2604    21931    migrations id    DEFAULT     n   ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);
 <   ALTER TABLE public.migrations ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    213    212            -           2604    21932    notification notification_id    DEFAULT     �   ALTER TABLE ONLY public.notification ALTER COLUMN notification_id SET DEFAULT nextval('public.notification_notification_id_seq'::regclass);
 K   ALTER TABLE public.notification ALTER COLUMN notification_id DROP DEFAULT;
       public       postgres    false    215    214            .           2604    21933    pegawai pegawai_id    DEFAULT     x   ALTER TABLE ONLY public.pegawai ALTER COLUMN pegawai_id SET DEFAULT nextval('public.pegawai_pegawai_id_seq'::regclass);
 A   ALTER TABLE public.pegawai ALTER COLUMN pegawai_id DROP DEFAULT;
       public       postgres    false    218    217            /           2604    21934    prodi prodi_id    DEFAULT     p   ALTER TABLE ONLY public.prodi ALTER COLUMN prodi_id SET DEFAULT nextval('public.prodi_prodi_id_seq'::regclass);
 =   ALTER TABLE public.prodi ALTER COLUMN prodi_id DROP DEFAULT;
       public       postgres    false    220    219            0           2604    21935    quiz quiz_id    DEFAULT     l   ALTER TABLE ONLY public.quiz ALTER COLUMN quiz_id SET DEFAULT nextval('public.quiz_quiz_id_seq'::regclass);
 ;   ALTER TABLE public.quiz ALTER COLUMN quiz_id DROP DEFAULT;
       public       postgres    false    222    221            1           2604    21936    rencana_studi id_rencana    DEFAULT     �   ALTER TABLE ONLY public.rencana_studi ALTER COLUMN id_rencana SET DEFAULT nextval('public.rencana_studi_id_rencana_seq'::regclass);
 G   ALTER TABLE public.rencana_studi ALTER COLUMN id_rencana DROP DEFAULT;
       public       postgres    false    224    223            2           2604    21937    report report_id    DEFAULT     t   ALTER TABLE ONLY public.report ALTER COLUMN report_id SET DEFAULT nextval('public.report_report_id_seq'::regclass);
 ?   ALTER TABLE public.report ALTER COLUMN report_id DROP DEFAULT;
       public       postgres    false    226    225            3           2604    21938    schedule schedule_id    DEFAULT     |   ALTER TABLE ONLY public.schedule ALTER COLUMN schedule_id SET DEFAULT nextval('public.schedule_schedule_id_seq'::regclass);
 C   ALTER TABLE public.schedule ALTER COLUMN schedule_id DROP DEFAULT;
       public       postgres    false    230    227            5           2604    21939    schedule_request request_id    DEFAULT     �   ALTER TABLE ONLY public.schedule_request ALTER COLUMN request_id SET DEFAULT nextval('public.schedule_request_request_id_seq'::regclass);
 J   ALTER TABLE public.schedule_request ALTER COLUMN request_id DROP DEFAULT;
       public       postgres    false    229    228            6           2604    21940    t_attendance id    DEFAULT     r   ALTER TABLE ONLY public.t_attendance ALTER COLUMN id SET DEFAULT nextval('public.t_attendance_id_seq'::regclass);
 >   ALTER TABLE public.t_attendance ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    232    231            7           2604    21941    t_chat chat_id    DEFAULT     p   ALTER TABLE ONLY public.t_chat ALTER COLUMN chat_id SET DEFAULT nextval('public.t_chat_chat_id_seq'::regclass);
 =   ALTER TABLE public.t_chat ALTER COLUMN chat_id DROP DEFAULT;
       public       postgres    false    234    233            8           2604    21942    t_inventory id    DEFAULT     p   ALTER TABLE ONLY public.t_inventory ALTER COLUMN id SET DEFAULT nextval('public.t_inventory_id_seq'::regclass);
 =   ALTER TABLE public.t_inventory ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    236    235            9           2604    21943    t_notification id    DEFAULT     v   ALTER TABLE ONLY public.t_notification ALTER COLUMN id SET DEFAULT nextval('public.t_notification_id_seq'::regclass);
 @   ALTER TABLE public.t_notification ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    238    237            :           2604    21944    t_schedule id    DEFAULT     n   ALTER TABLE ONLY public.t_schedule ALTER COLUMN id SET DEFAULT nextval('public.t_schedule_id_seq'::regclass);
 <   ALTER TABLE public.t_schedule ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    240    239            ;           2604    21945    tahun_akademik tahun_id    DEFAULT     �   ALTER TABLE ONLY public.tahun_akademik ALTER COLUMN tahun_id SET DEFAULT nextval('public.tahun_akademik_tahun_id_seq'::regclass);
 F   ALTER TABLE public.tahun_akademik ALTER COLUMN tahun_id DROP DEFAULT;
       public       postgres    false    242    241            <           2604    21946    tugas tugas_id    DEFAULT     p   ALTER TABLE ONLY public.tugas ALTER COLUMN tugas_id SET DEFAULT nextval('public.tugas_tugas_id_seq'::regclass);
 =   ALTER TABLE public.tugas ALTER COLUMN tugas_id DROP DEFAULT;
       public       postgres    false    244    243            =           2604    21947    users id    DEFAULT     d   ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);
 7   ALTER TABLE public.users ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    246    245            >           2604    21948    virtual virtual_id    DEFAULT     x   ALTER TABLE ONLY public.virtual ALTER COLUMN virtual_id SET DEFAULT nextval('public.virtual_virtual_id_seq'::regclass);
 A   ALTER TABLE public.virtual ALTER COLUMN virtual_id DROP DEFAULT;
       public       postgres    false    249    248                      0    21759    berita_acara 
   TABLE DATA               f   COPY public.berita_acara (id, t_schedule_id, voice_created, voice_location, ba_generated) FROM stdin;
    public       postgres    false    196   ?                0    21767    chat 
   TABLE DATA               `   COPY public.chat (chatroom_id, chatroom_admin, chatroom_created, chatroom_modified) FROM stdin;
    public       postgres    false    198   �                0    21772    chat_participant 
   TABLE DATA               O   COPY public.chat_participant (id, chat_chatroom_id, user_username) FROM stdin;
    public       postgres    false    200   �                0    21777 	   classroom 
   TABLE DATA               ]   COPY public.classroom (classroom_id, classroom_name, classroom_loc, building_id) FROM stdin;
    public       postgres    false    202   �                0    21782 	   inventory 
   TABLE DATA               n   COPY public.inventory (inventory_id, inventory_name, schedule, last_check, classroom_classroomid) FROM stdin;
    public       postgres    false    204   4	                0    21787 	   mahasiswa 
   TABLE DATA               e   COPY public.mahasiswa (mahasiswa_id, nim, mahasiswa_name, mahasiswa_type, user_username) FROM stdin;
    public       postgres    false    206   ~	                0    21792    mata_kuliah 
   TABLE DATA               Y   COPY public.mata_kuliah (mata_kuliah_id, mata_kuliah, kode_mata_kuliah, sks) FROM stdin;
    public       postgres    false    208   �	                0    21797    media_sharing 
   TABLE DATA               �   COPY public.media_sharing (media_id, media_type, media_name, media_location, media_added, media_modified, accesibility, download_count, user_username, mata_kuliah_mata_kuliah_id) FROM stdin;
    public       postgres    false    210   
                0    21805 
   migrations 
   TABLE DATA               :   COPY public.migrations (id, migration, batch) FROM stdin;
    public       postgres    false    212   �
                 0    21810    notification 
   TABLE DATA               ]   COPY public.notification (notification_id, notification_type, notification_name) FROM stdin;
    public       postgres    false    214   �
      "          0    21815    password_resets 
   TABLE DATA               C   COPY public.password_resets (email, token, created_at) FROM stdin;
    public       postgres    false    216         #          0    21821    pegawai 
   TABLE DATA               k   COPY public.pegawai (pegawai_id, nip, pegawai_name, pangkat, jabatan, is_dosen, user_username) FROM stdin;
    public       postgres    false    217   (      %          0    21826    prodi 
   TABLE DATA               ?   COPY public.prodi (prodi_id, prodi_name, fakultas) FROM stdin;
    public       postgres    false    219   �      '          0    21834    quiz 
   TABLE DATA               2   COPY public.quiz (quiz_id, quiz_name) FROM stdin;
    public       postgres    false    221   �      )          0    21839    rencana_studi 
   TABLE DATA               �   COPY public.rencana_studi (id_rencana, tahun_akademik_tahun_id, prodi_prodi_id, mata_kuliah_mata_kuliah_id, pegawai_pegawai_id, default_time, default_date) FROM stdin;
    public       postgres    false    223   �      +          0    21844    report 
   TABLE DATA               b   COPY public.report (report_id, report_type, report_title, report_created, report_loc) FROM stdin;
    public       postgres    false    225   "      -          0    21852    schedule 
   TABLE DATA               f   COPY public.schedule (schedule_id, schedule_date, schedule_time_start, schedule_time_end) FROM stdin;
    public       postgres    false    227   ?      .          0    21855    schedule_request 
   TABLE DATA               c   COPY public.schedule_request (request_id, status_request, user_username, time_request) FROM stdin;
    public       postgres    false    228   |      1          0    21863    t_attendance 
   TABLE DATA               �   COPY public.t_attendance (id, mahasiswa_mahasiswa_id, t_schedule_id, attendance, screen_created, screen_value, tugas_tugas_id, tugas_loc, nilai_tugas, quiz_quiz_id, quiz_loc, nilai_quiz) FROM stdin;
    public       postgres    false    231   �      3          0    21871    t_chat 
   TABLE DATA               f   COPY public.t_chat (chat_id, chat_chatroom_id, text_chat, chat_time, root, user_username) FROM stdin;
    public       postgres    false    233   �      5          0    21876    t_inventory 
   TABLE DATA               �   COPY public.t_inventory (id, inventory_status, inventory_jumlah, image_loc, jumlah_byimage, image_taken, inventory_inventoryid) FROM stdin;
    public       postgres    false    235   p      7          0    21881    t_notification 
   TABLE DATA               �   COPY public.t_notification (id, notification_notification_id, notification_value, notification_created, notification_channel, is_read, user_username) FROM stdin;
    public       postgres    false    237   �      9          0    21886 
   t_schedule 
   TABLE DATA               �   COPY public.t_schedule (id, schedule_schedule_id, rencana_studi_id_rencana, classroom_classroom_id, schedule_request_request_id, studi_type, studi_value, virtual_virtual_id) FROM stdin;
    public       postgres    false    239   �      ;          0    21891    tahun_akademik 
   TABLE DATA               C   COPY public.tahun_akademik (tahun_id, tahun, semester) FROM stdin;
    public       postgres    false    241   �      =          0    21896    tugas 
   TABLE DATA               T   COPY public.tugas (tugas_id, tugas_name, tugas_created, tugas_deadline) FROM stdin;
    public       postgres    false    243         ?          0    21901    users 
   TABLE DATA               u   COPY public.users (id, name, email, email_verified_at, password, remember_token, created_at, updated_at) FROM stdin;
    public       postgres    false    245   0      A          0    21909    usersz 
   TABLE DATA               �   COPY public.usersz (username, biometric, email, token, created_at, updated_at, role_id, name, email_verified_at, password) FROM stdin;
    public       postgres    false    247   �      B          0    21915    virtual 
   TABLE DATA               G   COPY public.virtual (virtual_id, virtual_name, enroll_key) FROM stdin;
    public       postgres    false    248   6      d           0    0    berita_acara_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.berita_acara_id_seq', 2, true);
            public       postgres    false    197            e           0    0    chat_chatroom_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.chat_chatroom_id_seq', 2, true);
            public       postgres    false    199            f           0    0    chat_participant_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.chat_participant_id_seq', 1, false);
            public       postgres    false    201            g           0    0    classroom_classroom_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.classroom_classroom_id_seq', 1, false);
            public       postgres    false    203            h           0    0    inventory_inventory_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.inventory_inventory_id_seq', 1, false);
            public       postgres    false    205            i           0    0    mahasiswa_mahasiswa_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.mahasiswa_mahasiswa_id_seq', 1, true);
            public       postgres    false    207            j           0    0    mata_kuliah_mata_kuliah_id_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.mata_kuliah_mata_kuliah_id_seq', 1, true);
            public       postgres    false    209            k           0    0    media_sharing_media_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.media_sharing_media_id_seq', 1, false);
            public       postgres    false    211            l           0    0    migrations_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.migrations_id_seq', 2, true);
            public       postgres    false    213            m           0    0     notification_notification_id_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public.notification_notification_id_seq', 1, false);
            public       postgres    false    215            n           0    0    pegawai_pegawai_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.pegawai_pegawai_id_seq', 1, true);
            public       postgres    false    218            o           0    0    prodi_prodi_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.prodi_prodi_id_seq', 1, true);
            public       postgres    false    220            p           0    0    quiz_quiz_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.quiz_quiz_id_seq', 1, false);
            public       postgres    false    222            q           0    0    rencana_studi_id_rencana_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.rencana_studi_id_rencana_seq', 1, true);
            public       postgres    false    224            r           0    0    report_report_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.report_report_id_seq', 1, false);
            public       postgres    false    226            s           0    0    schedule_request_request_id_seq    SEQUENCE SET     N   SELECT pg_catalog.setval('public.schedule_request_request_id_seq', 1, false);
            public       postgres    false    229            t           0    0    schedule_schedule_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.schedule_schedule_id_seq', 1, true);
            public       postgres    false    230            u           0    0    t_attendance_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.t_attendance_id_seq', 1, true);
            public       postgres    false    232            v           0    0    t_chat_chat_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.t_chat_chat_id_seq', 4, true);
            public       postgres    false    234            w           0    0    t_inventory_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.t_inventory_id_seq', 1, false);
            public       postgres    false    236            x           0    0    t_notification_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.t_notification_id_seq', 1, false);
            public       postgres    false    238            y           0    0    t_schedule_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.t_schedule_id_seq', 1, true);
            public       postgres    false    240            z           0    0    tahun_akademik_tahun_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.tahun_akademik_tahun_id_seq', 2, true);
            public       postgres    false    242            {           0    0    tugas_tugas_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.tugas_tugas_id_seq', 1, false);
            public       postgres    false    244            |           0    0    users_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.users_id_seq', 1, true);
            public       postgres    false    246            }           0    0    virtual_virtual_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.virtual_virtual_id_seq', 1, false);
            public       postgres    false    249            @           2606    21950    berita_acara berita_acara_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.berita_acara
    ADD CONSTRAINT berita_acara_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.berita_acara DROP CONSTRAINT berita_acara_pkey;
       public         postgres    false    196            D           2606    21952 &   chat_participant chat_participant_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.chat_participant
    ADD CONSTRAINT chat_participant_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.chat_participant DROP CONSTRAINT chat_participant_pkey;
       public         postgres    false    200            B           2606    21954    chat chat_pkey 
   CONSTRAINT     U   ALTER TABLE ONLY public.chat
    ADD CONSTRAINT chat_pkey PRIMARY KEY (chatroom_id);
 8   ALTER TABLE ONLY public.chat DROP CONSTRAINT chat_pkey;
       public         postgres    false    198            F           2606    21956    classroom classroom_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.classroom
    ADD CONSTRAINT classroom_pkey PRIMARY KEY (classroom_id);
 B   ALTER TABLE ONLY public.classroom DROP CONSTRAINT classroom_pkey;
       public         postgres    false    202            H           2606    21958    inventory inventory_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.inventory
    ADD CONSTRAINT inventory_pkey PRIMARY KEY (inventory_id);
 B   ALTER TABLE ONLY public.inventory DROP CONSTRAINT inventory_pkey;
       public         postgres    false    204            J           2606    21960    mahasiswa mahasiswa_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.mahasiswa
    ADD CONSTRAINT mahasiswa_pkey PRIMARY KEY (mahasiswa_id);
 B   ALTER TABLE ONLY public.mahasiswa DROP CONSTRAINT mahasiswa_pkey;
       public         postgres    false    206            L           2606    21962    mata_kuliah mata_kuliah_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.mata_kuliah
    ADD CONSTRAINT mata_kuliah_pkey PRIMARY KEY (mata_kuliah_id);
 F   ALTER TABLE ONLY public.mata_kuliah DROP CONSTRAINT mata_kuliah_pkey;
       public         postgres    false    208            N           2606    21964     media_sharing media_sharing_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.media_sharing
    ADD CONSTRAINT media_sharing_pkey PRIMARY KEY (media_id);
 J   ALTER TABLE ONLY public.media_sharing DROP CONSTRAINT media_sharing_pkey;
       public         postgres    false    210            P           2606    21966    migrations migrations_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.migrations DROP CONSTRAINT migrations_pkey;
       public         postgres    false    212            R           2606    21968    notification notification_pkey 
   CONSTRAINT     i   ALTER TABLE ONLY public.notification
    ADD CONSTRAINT notification_pkey PRIMARY KEY (notification_id);
 H   ALTER TABLE ONLY public.notification DROP CONSTRAINT notification_pkey;
       public         postgres    false    214            U           2606    21970    pegawai pegawai_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.pegawai
    ADD CONSTRAINT pegawai_pkey PRIMARY KEY (pegawai_id);
 >   ALTER TABLE ONLY public.pegawai DROP CONSTRAINT pegawai_pkey;
       public         postgres    false    217            W           2606    21972    prodi prodi_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.prodi
    ADD CONSTRAINT prodi_pkey PRIMARY KEY (prodi_id);
 :   ALTER TABLE ONLY public.prodi DROP CONSTRAINT prodi_pkey;
       public         postgres    false    219            Y           2606    21974    quiz quiz_pkey 
   CONSTRAINT     Q   ALTER TABLE ONLY public.quiz
    ADD CONSTRAINT quiz_pkey PRIMARY KEY (quiz_id);
 8   ALTER TABLE ONLY public.quiz DROP CONSTRAINT quiz_pkey;
       public         postgres    false    221            [           2606    21976     rencana_studi rencana_studi_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.rencana_studi
    ADD CONSTRAINT rencana_studi_pkey PRIMARY KEY (id_rencana);
 J   ALTER TABLE ONLY public.rencana_studi DROP CONSTRAINT rencana_studi_pkey;
       public         postgres    false    223            ]           2606    21978    report report_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY public.report
    ADD CONSTRAINT report_pkey PRIMARY KEY (report_id);
 <   ALTER TABLE ONLY public.report DROP CONSTRAINT report_pkey;
       public         postgres    false    225            _           2606    21980    schedule schedule_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY public.schedule
    ADD CONSTRAINT schedule_pkey PRIMARY KEY (schedule_id);
 @   ALTER TABLE ONLY public.schedule DROP CONSTRAINT schedule_pkey;
       public         postgres    false    227            a           2606    21982 &   schedule_request schedule_request_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.schedule_request
    ADD CONSTRAINT schedule_request_pkey PRIMARY KEY (request_id);
 P   ALTER TABLE ONLY public.schedule_request DROP CONSTRAINT schedule_request_pkey;
       public         postgres    false    228            c           2606    21984    t_attendance t_attendance_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.t_attendance
    ADD CONSTRAINT t_attendance_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.t_attendance DROP CONSTRAINT t_attendance_pkey;
       public         postgres    false    231            e           2606    21986    t_chat t_chat_pkey 
   CONSTRAINT     U   ALTER TABLE ONLY public.t_chat
    ADD CONSTRAINT t_chat_pkey PRIMARY KEY (chat_id);
 <   ALTER TABLE ONLY public.t_chat DROP CONSTRAINT t_chat_pkey;
       public         postgres    false    233            g           2606    21988    t_inventory t_inventory_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.t_inventory
    ADD CONSTRAINT t_inventory_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.t_inventory DROP CONSTRAINT t_inventory_pkey;
       public         postgres    false    235            i           2606    21990 "   t_notification t_notification_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.t_notification
    ADD CONSTRAINT t_notification_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.t_notification DROP CONSTRAINT t_notification_pkey;
       public         postgres    false    237            k           2606    21992    t_schedule t_schedule_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.t_schedule
    ADD CONSTRAINT t_schedule_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.t_schedule DROP CONSTRAINT t_schedule_pkey;
       public         postgres    false    239            m           2606    21994 "   tahun_akademik tahun_akademik_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.tahun_akademik
    ADD CONSTRAINT tahun_akademik_pkey PRIMARY KEY (tahun_id);
 L   ALTER TABLE ONLY public.tahun_akademik DROP CONSTRAINT tahun_akademik_pkey;
       public         postgres    false    241            o           2606    21996    tugas tugas_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.tugas
    ADD CONSTRAINT tugas_pkey PRIMARY KEY (tugas_id);
 :   ALTER TABLE ONLY public.tugas DROP CONSTRAINT tugas_pkey;
       public         postgres    false    243            u           2606    21998    usersz user_biometric_key 
   CONSTRAINT     Y   ALTER TABLE ONLY public.usersz
    ADD CONSTRAINT user_biometric_key UNIQUE (biometric);
 C   ALTER TABLE ONLY public.usersz DROP CONSTRAINT user_biometric_key;
       public         postgres    false    247            w           2606    22000    usersz user_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.usersz
    ADD CONSTRAINT user_pkey PRIMARY KEY (username);
 :   ALTER TABLE ONLY public.usersz DROP CONSTRAINT user_pkey;
       public         postgres    false    247            q           2606    22002    users users_email_unique 
   CONSTRAINT     T   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);
 B   ALTER TABLE ONLY public.users DROP CONSTRAINT users_email_unique;
       public         postgres    false    245            s           2606    22004    users users_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public         postgres    false    245            y           2606    22006    virtual virtual_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.virtual
    ADD CONSTRAINT virtual_pkey PRIMARY KEY (virtual_id);
 >   ALTER TABLE ONLY public.virtual DROP CONSTRAINT virtual_pkey;
       public         postgres    false    248            S           1259    22007    password_resets_email_index    INDEX     X   CREATE INDEX password_resets_email_index ON public.password_resets USING btree (email);
 /   DROP INDEX public.password_resets_email_index;
       public         postgres    false    216            z           2606    22008    berita_acara fkberita_aca811372    FK CONSTRAINT     �   ALTER TABLE ONLY public.berita_acara
    ADD CONSTRAINT fkberita_aca811372 FOREIGN KEY (t_schedule_id) REFERENCES public.t_schedule(id);
 I   ALTER TABLE ONLY public.berita_acara DROP CONSTRAINT fkberita_aca811372;
       public       postgres    false    239    196    2923            {           2606    22013 #   chat_participant fkchat_parti710993    FK CONSTRAINT     �   ALTER TABLE ONLY public.chat_participant
    ADD CONSTRAINT fkchat_parti710993 FOREIGN KEY (chat_chatroom_id) REFERENCES public.chat(chatroom_id);
 M   ALTER TABLE ONLY public.chat_participant DROP CONSTRAINT fkchat_parti710993;
       public       postgres    false    198    200    2882            |           2606    22018 #   chat_participant fkchat_parti836424    FK CONSTRAINT     �   ALTER TABLE ONLY public.chat_participant
    ADD CONSTRAINT fkchat_parti836424 FOREIGN KEY (user_username) REFERENCES public.usersz(username);
 M   ALTER TABLE ONLY public.chat_participant DROP CONSTRAINT fkchat_parti836424;
       public       postgres    false    247    200    2935            }           2606    22023    inventory fkinventory253906    FK CONSTRAINT     �   ALTER TABLE ONLY public.inventory
    ADD CONSTRAINT fkinventory253906 FOREIGN KEY (classroom_classroomid) REFERENCES public.classroom(classroom_id);
 E   ALTER TABLE ONLY public.inventory DROP CONSTRAINT fkinventory253906;
       public       postgres    false    2886    202    204            ~           2606    22028    mahasiswa fkmahasiswa831244    FK CONSTRAINT     �   ALTER TABLE ONLY public.mahasiswa
    ADD CONSTRAINT fkmahasiswa831244 FOREIGN KEY (user_username) REFERENCES public.usersz(username);
 E   ALTER TABLE ONLY public.mahasiswa DROP CONSTRAINT fkmahasiswa831244;
       public       postgres    false    247    206    2935                       2606    22033     media_sharing fkmedia_shar223295    FK CONSTRAINT     �   ALTER TABLE ONLY public.media_sharing
    ADD CONSTRAINT fkmedia_shar223295 FOREIGN KEY (mata_kuliah_mata_kuliah_id) REFERENCES public.mata_kuliah(mata_kuliah_id);
 J   ALTER TABLE ONLY public.media_sharing DROP CONSTRAINT fkmedia_shar223295;
       public       postgres    false    2892    210    208            �           2606    22038     media_sharing fkmedia_shar484449    FK CONSTRAINT     �   ALTER TABLE ONLY public.media_sharing
    ADD CONSTRAINT fkmedia_shar484449 FOREIGN KEY (user_username) REFERENCES public.usersz(username);
 J   ALTER TABLE ONLY public.media_sharing DROP CONSTRAINT fkmedia_shar484449;
       public       postgres    false    2935    210    247            �           2606    22043    pegawai fkpegawai406195    FK CONSTRAINT     �   ALTER TABLE ONLY public.pegawai
    ADD CONSTRAINT fkpegawai406195 FOREIGN KEY (user_username) REFERENCES public.usersz(username);
 A   ALTER TABLE ONLY public.pegawai DROP CONSTRAINT fkpegawai406195;
       public       postgres    false    247    217    2935            �           2606    22048     rencana_studi fkrencana_st147627    FK CONSTRAINT     �   ALTER TABLE ONLY public.rencana_studi
    ADD CONSTRAINT fkrencana_st147627 FOREIGN KEY (pegawai_pegawai_id) REFERENCES public.pegawai(pegawai_id);
 J   ALTER TABLE ONLY public.rencana_studi DROP CONSTRAINT fkrencana_st147627;
       public       postgres    false    223    217    2901            �           2606    22053     rencana_studi fkrencana_st831142    FK CONSTRAINT     �   ALTER TABLE ONLY public.rencana_studi
    ADD CONSTRAINT fkrencana_st831142 FOREIGN KEY (mata_kuliah_mata_kuliah_id) REFERENCES public.mata_kuliah(mata_kuliah_id);
 J   ALTER TABLE ONLY public.rencana_studi DROP CONSTRAINT fkrencana_st831142;
       public       postgres    false    223    208    2892            �           2606    22058     rencana_studi fkrencana_st895633    FK CONSTRAINT     �   ALTER TABLE ONLY public.rencana_studi
    ADD CONSTRAINT fkrencana_st895633 FOREIGN KEY (prodi_prodi_id) REFERENCES public.prodi(prodi_id);
 J   ALTER TABLE ONLY public.rencana_studi DROP CONSTRAINT fkrencana_st895633;
       public       postgres    false    223    219    2903            �           2606    22063     rencana_studi fkrencana_st960815    FK CONSTRAINT     �   ALTER TABLE ONLY public.rencana_studi
    ADD CONSTRAINT fkrencana_st960815 FOREIGN KEY (tahun_akademik_tahun_id) REFERENCES public.tahun_akademik(tahun_id);
 J   ALTER TABLE ONLY public.rencana_studi DROP CONSTRAINT fkrencana_st960815;
       public       postgres    false    241    2925    223            �           2606    22068 #   schedule_request fkschedule_r667801    FK CONSTRAINT     �   ALTER TABLE ONLY public.schedule_request
    ADD CONSTRAINT fkschedule_r667801 FOREIGN KEY (user_username) REFERENCES public.usersz(username);
 M   ALTER TABLE ONLY public.schedule_request DROP CONSTRAINT fkschedule_r667801;
       public       postgres    false    228    2935    247            �           2606    22073    t_attendance fkt_attendan519853    FK CONSTRAINT     �   ALTER TABLE ONLY public.t_attendance
    ADD CONSTRAINT fkt_attendan519853 FOREIGN KEY (mahasiswa_mahasiswa_id) REFERENCES public.mahasiswa(mahasiswa_id);
 I   ALTER TABLE ONLY public.t_attendance DROP CONSTRAINT fkt_attendan519853;
       public       postgres    false    231    2890    206            �           2606    22078    t_attendance fkt_attendan531379    FK CONSTRAINT     �   ALTER TABLE ONLY public.t_attendance
    ADD CONSTRAINT fkt_attendan531379 FOREIGN KEY (quiz_quiz_id) REFERENCES public.quiz(quiz_id);
 I   ALTER TABLE ONLY public.t_attendance DROP CONSTRAINT fkt_attendan531379;
       public       postgres    false    2905    231    221            �           2606    22083    t_attendance fkt_attendan708236    FK CONSTRAINT     �   ALTER TABLE ONLY public.t_attendance
    ADD CONSTRAINT fkt_attendan708236 FOREIGN KEY (t_schedule_id) REFERENCES public.t_schedule(id);
 I   ALTER TABLE ONLY public.t_attendance DROP CONSTRAINT fkt_attendan708236;
       public       postgres    false    239    2923    231            �           2606    22088    t_attendance fkt_attendan819738    FK CONSTRAINT     �   ALTER TABLE ONLY public.t_attendance
    ADD CONSTRAINT fkt_attendan819738 FOREIGN KEY (tugas_tugas_id) REFERENCES public.tugas(tugas_id);
 I   ALTER TABLE ONLY public.t_attendance DROP CONSTRAINT fkt_attendan819738;
       public       postgres    false    243    231    2927            �           2606    22093    t_chat fkt_chat476578    FK CONSTRAINT     �   ALTER TABLE ONLY public.t_chat
    ADD CONSTRAINT fkt_chat476578 FOREIGN KEY (user_username) REFERENCES public.usersz(username);
 ?   ALTER TABLE ONLY public.t_chat DROP CONSTRAINT fkt_chat476578;
       public       postgres    false    247    2935    233            �           2606    22098    t_chat fkt_chat602009    FK CONSTRAINT     �   ALTER TABLE ONLY public.t_chat
    ADD CONSTRAINT fkt_chat602009 FOREIGN KEY (chat_chatroom_id) REFERENCES public.chat(chatroom_id);
 ?   ALTER TABLE ONLY public.t_chat DROP CONSTRAINT fkt_chat602009;
       public       postgres    false    2882    198    233            �           2606    22103    t_inventory fkt_inventor135334    FK CONSTRAINT     �   ALTER TABLE ONLY public.t_inventory
    ADD CONSTRAINT fkt_inventor135334 FOREIGN KEY (inventory_inventoryid) REFERENCES public.inventory(inventory_id);
 H   ALTER TABLE ONLY public.t_inventory DROP CONSTRAINT fkt_inventor135334;
       public       postgres    false    2888    235    204            �           2606    22108 !   t_notification fkt_notifica784051    FK CONSTRAINT     �   ALTER TABLE ONLY public.t_notification
    ADD CONSTRAINT fkt_notifica784051 FOREIGN KEY (user_username) REFERENCES public.usersz(username);
 K   ALTER TABLE ONLY public.t_notification DROP CONSTRAINT fkt_notifica784051;
       public       postgres    false    2935    247    237            �           2606    22113 !   t_notification fkt_notifica787324    FK CONSTRAINT     �   ALTER TABLE ONLY public.t_notification
    ADD CONSTRAINT fkt_notifica787324 FOREIGN KEY (notification_notification_id) REFERENCES public.notification(notification_id);
 K   ALTER TABLE ONLY public.t_notification DROP CONSTRAINT fkt_notifica787324;
       public       postgres    false    2898    237    214            �           2606    22118    t_schedule fkt_schedule155885    FK CONSTRAINT     �   ALTER TABLE ONLY public.t_schedule
    ADD CONSTRAINT fkt_schedule155885 FOREIGN KEY (virtual_virtual_id) REFERENCES public.virtual(virtual_id);
 G   ALTER TABLE ONLY public.t_schedule DROP CONSTRAINT fkt_schedule155885;
       public       postgres    false    248    2937    239            �           2606    22123    t_schedule fkt_schedule195384    FK CONSTRAINT     �   ALTER TABLE ONLY public.t_schedule
    ADD CONSTRAINT fkt_schedule195384 FOREIGN KEY (classroom_classroom_id) REFERENCES public.classroom(classroom_id);
 G   ALTER TABLE ONLY public.t_schedule DROP CONSTRAINT fkt_schedule195384;
       public       postgres    false    239    2886    202            �           2606    22128    t_schedule fkt_schedule56399    FK CONSTRAINT     �   ALTER TABLE ONLY public.t_schedule
    ADD CONSTRAINT fkt_schedule56399 FOREIGN KEY (rencana_studi_id_rencana) REFERENCES public.rencana_studi(id_rencana);
 F   ALTER TABLE ONLY public.t_schedule DROP CONSTRAINT fkt_schedule56399;
       public       postgres    false    239    2907    223            �           2606    22133    t_schedule fkt_schedule689458    FK CONSTRAINT     �   ALTER TABLE ONLY public.t_schedule
    ADD CONSTRAINT fkt_schedule689458 FOREIGN KEY (schedule_request_request_id) REFERENCES public.schedule_request(request_id);
 G   ALTER TABLE ONLY public.t_schedule DROP CONSTRAINT fkt_schedule689458;
       public       postgres    false    2913    228    239            �           2606    22138    t_schedule fkt_schedule901683    FK CONSTRAINT     �   ALTER TABLE ONLY public.t_schedule
    ADD CONSTRAINT fkt_schedule901683 FOREIGN KEY (schedule_schedule_id) REFERENCES public.schedule(schedule_id);
 G   ALTER TABLE ONLY public.t_schedule DROP CONSTRAINT fkt_schedule901683;
       public       postgres    false    2911    239    227               :   x�3�4�420��50�54Q04�25�24�60I(�g�'*8%�*x%�&er��qqq �'         .   x�3�,�L�H�420��50�54V00�20�2��60�!����� }Q�            x������ � �         @   x�3�*M�KWN���K,R0��".#4qc��1T�%5;1*f�铘���������� ��h         :   x�3�����,�/��!C.#΀����ld1c� g������Ē��<��W� �h         +   x�3�4262�004��L�HU(J��,I�4�,�b���� �E	�         ?   x�3�H-J�KN�KO�S�I�L��!��y�9��
�yi�E��ř��>�FF��1~\1z\\\ :��         ~   x�]�K
�0 �ur�^ �1��Ju��`�4h&!o�k����
��#ug� ���'�W򡳴�ql�5o'�Zk��.�(S�*�9�2{0 Geǋ]N�
{����i8���y^�����A,)�         H   x�3�4204�74�74�7 ����Ē����Ԣ���Ĥ�TNC.#d��(
����R�R�SKZb���� ]             x������ � �      "      x������ � �      #   r   x�3�4�4364244��4004400�(�O�Sp)�Sx�0�3/]O��.�H,J��Q���H�M��KT-I�MT�-MIT���O���I�.�/R�N-H�I�,�,�h����� �3 G      %   #   x�3�I����Vp�I�.)��q������ u��      '      x������ � �      )      x�3�4�4�? ����� "�3      +      x������ � �      -   -   x�3�420��50�54�44�20 "NC3��.md�E:F��� �2�      .      x�3�,�,�L�H������� -�8      1   /   x�3�4�N#CK]C]CCc+C+ms�?L����� 4�
�      3   {   x�e�1
�0��Y:���"�2i}�f��E��S2��U������10\uuU��x�KK�rpĉ}��P��y�����e���c�1ò�#�=�}�8�$1���5�(�����'D|�e&_      5      x�3�,�4��"C.#T#�=... �{5      7      x������ � �      9      x�3�4�?��e�i����qqq �-#      ;      x�3�420��4�2�0��b���� )=      =      x������ � �      ?   �   x�3��L�HU(J��,I�,���2K����2S8�-uLu���LL���8U�*UT�����=s��
]�<"������K�B�-�|���3"�K]��<���}�3���r9c���	3V02�2��25������ �**�      A   W  x�u��n�P���]�kB��)���,���� D.^x���M�d�L��7S^�8�6*Uv0��&��b��g��IB��q��ZX\c�.�E��@�	�1v�n�*�\�o�Pm���M&�x0���g0�Š��EWM�a�u7�� J~�rf�Wg�͐"�\�����2X����F$a˕���n�౲��n���~���^�mr�������*@ z�P�~�(�^
��W[ò.��,�^�I����r7���YH��c��)��L��	����P}��;>^J�#M`�����"m9T���e�Hvߗ��"�P��Rs����9�6�7M�镙      B      x������ � �     
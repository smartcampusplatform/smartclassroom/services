<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $mahasiswa_id
 * @property string $user_username
 * @property string $nim
 * @property string $mahasiswa_name
 * @property int $mahasiswa_type
 * @property User $user
 * @property TAttendance[] $tAttendances
 */
class Mahasiswa extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'mahasiswa';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'mahasiswa_id';

    /**
     * @var array
     */
    protected $fillable = ['user_username', 'nim', 'mahasiswa_name', 'mahasiswa_type'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_username', 'username');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tAttendances()
    {
        return $this->hasMany('App\Models\TAttendance', 'mahasiswa_mahasiswa_id', 'mahasiswa_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $prodi_id
 * @property string $prodi_name
 * @property string $fakultas
 * @property RencanaStudi[] $rencanaStudis
 */
class Prodi extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'prodi';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'prodi_id';

    /**
     * @var array
     */
    protected $fillable = ['prodi_name', 'fakultas'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rencanaStudis()
    {
        return $this->hasMany('App\Models\RencanaStudi', 'prodi_prodi_id', 'prodi_id');
    }
}

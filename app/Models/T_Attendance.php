<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $mahasiswa_mahasiswa_id
 * @property int $t_schedule_id
 * @property int $tugas_tugas_id
 * @property int $quiz_quiz_id
 * @property boolean $attendance
 * @property string $screen_created
 * @property boolean $screen_value
 * @property string $tugas_loc
 * @property integer $nilai_tugas
 * @property string $quiz_loc
 * @property integer $nilai_quiz
 * @property Mahasiswa $mahasiswa
 * @property TSchedule $tSchedule
 * @property Tuga $tuga
 * @property Quiz $quiz
 */
class T_Attendance extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    public $timestamps = false;
    protected $table = 't_attendance';

    /**
     * @var array
     */

    protected $fillable = ['mahasiswa_mahasiswa_id', 't_schedule_id', 'tugas_tugas_id', 'quiz_quiz_id', 'attendance', 'screen_created', 'screen_value', 'tugas_loc', 'nilai_tugas', 'quiz_loc', 'nilai_quiz'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mahasiswa()
    {
        return $this->belongsTo('App\Models\Mahasiswa', 'mahasiswa_mahasiswa_id', 'mahasiswa_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tSchedule()
    {
        return $this->belongsTo('App\Models\TSchedule');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tuga()
    {
        return $this->belongsTo('App\Models\Tuga', 'tugas_tugas_id', 'tugas_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function quiz()
    {
        return $this->belongsTo('App\Models\Quiz', 'quiz_quiz_id', 'quiz_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $mata_kuliah_id
 * @property string $mata_kuliah
 * @property integer $sks
 * @property MediaSharing[] $mediaSharings
 * @property RencanaStudi[] $rencanaStudis
 */
class Mata_Kuliah extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'mata_kuliah';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'mata_kuliah_id';

    /**
     * @var array
     */
    protected $fillable = ['mata_kuliah', 'sks'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function mediaSharings()
    {
        return $this->hasMany('App\Models\MediaSharing', 'mata_kuliah_mata_kuliah_id', 'mata_kuliah_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rencanaStudis()
    {
        return $this->hasMany('App\Models\RencanaStudi', 'mata_kuliah_mata_kuliah_id', 'mata_kuliah_id');
    }
}

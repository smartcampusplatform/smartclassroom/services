<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $virtual_id
 * @property string $virtual_name
 * @property string $enroll_key
 * @property TSchedule[] $tSchedules
 */
class Virtual extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'virtual';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'virtual_id';

    /**
     * @var array
     */
    protected $fillable = ['virtual_name', 'enroll_key'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tSchedules()
    {
        return $this->hasMany('App\Models\TSchedule', 'virtual_virtual_id', 'virtual_id');
    }
}

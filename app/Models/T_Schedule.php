<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $schedule_schedule_id
 * @property int $rencana_studi_id_rencana
 * @property int $classroom_classroom_id
 * @property int $schedule_request_request_id
 * @property int $virtual_virtual_id
 * @property integer $studi_type
 * @property boolean $studi_value
 * @property Schedule $schedule
 * @property RencanaStudi $rencanaStudi
 * @property Classroom $classroom
 * @property ScheduleRequest $scheduleRequest
 * @property Virtual $virtual
 * @property TAttendance[] $tAttendances
 * @property BeritaAcara[] $beritaAcaras
 */
class T_Schedule extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 't_schedule';

    /**
     * @var array
     */
    protected $fillable = ['schedule_schedule_id', 'rencana_studi_id_rencana', 'classroom_classroom_id', 'schedule_request_request_id', 'virtual_virtual_id', 'studi_type', 'studi_value'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function schedule()
    {
        return $this->belongsTo('App\Models\Schedule', 'schedule_schedule_id', 'schedule_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function rencanaStudi()
    {
        return $this->belongsTo('App\Models\RencanaStudi', 'rencana_studi_id_rencana', 'id_rencana');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function classroom()
    {
        return $this->belongsTo('App\Models\Classroom', 'classroom_classroom_id', 'classroom_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function scheduleRequest()
    {
        return $this->belongsTo('App\Models\ScheduleRequest', 'schedule_request_request_id', 'request_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function virtual()
    {
        return $this->belongsTo('App\Models\Virtual', 'virtual_virtual_id', 'virtual_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tAttendances()
    {
        return $this->hasMany('App\Models\TAttendance');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function beritaAcaras()
    {
        return $this->hasMany('App\Models\BeritaAcara');
    }
}

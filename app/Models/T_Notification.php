<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $notification_notification_id
 * @property string $user_username
 * @property string $notification_value
 * @property string $notification_created
 * @property integer $notification_channel
 * @property boolean $is_read
 * @property Notification $notification
 * @property User $user
 */
class T_Notification extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 't_notification';

    /**
     * @var array
     */
    protected $fillable = ['notification_notification_id', 'user_username', 'notification_value', 'notification_created', 'notification_channel', 'is_read'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function notification()
    {
        return $this->belongsTo('App\Models\Notification', 'notification_notification_id', 'notification_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_username', 'username');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $schedule_id
 * @property string $schedule_date
 * @property string $schedule_time_start
 * @property string $schedule_time_end
 * @property TSchedule[] $tSchedules
 */
class Schedule extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'schedule';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'schedule_id';

    /**
     * @var array
     */
    protected $fillable = ['schedule_date', 'schedule_time_start', 'schedule_time_end'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tSchedules()
    {
        return $this->hasMany('App\Models\TSchedule', 'schedule_schedule_id', 'schedule_id');
    }
}

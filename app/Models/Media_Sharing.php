<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $media_id
 * @property string $user_username
 * @property int $mata_kuliah_mata_kuliah_id
 * @property integer $media_type
 * @property string $media_name
 * @property string $media_location
 * @property string $media_added
 * @property string $media_modified
 * @property boolean $accesibility
 * @property int $download_count
 * @property User $user
 * @property MataKuliah $mataKuliah
 */
class Media_Sharing extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'media_sharing';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'media_id';

    /**
     * @var array
     */
    protected $fillable = ['user_username', 'mata_kuliah_mata_kuliah_id', 'media_type', 'media_name', 'media_location', 'media_added', 'media_modified', 'accesibility', 'download_count'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_username', 'username');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mataKuliah()
    {
        return $this->belongsTo('App\Models\MataKuliah', 'mata_kuliah_mata_kuliah_id', 'mata_kuliah_id');
    }
}

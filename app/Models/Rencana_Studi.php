<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_rencana
 * @property int $tahun_akademik_tahun_id
 * @property int $prodi_prodi_id
 * @property int $mata_kuliah_mata_kuliah_id
 * @property int $pegawai_pegawai_id
 * @property int $schedule_schedule_id
 * @property TahunAkademik $tahunAkademik
 * @property Prodi $prodi
 * @property MataKuliah $mataKuliah
 * @property Pegawai $pegawai
 * @property TSchedule[] $tSchedules
 */
class Rencana_Studi extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'rencana_studi';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_rencana';

    /**
     * @var array
     */
    protected $fillable = ['tahun_akademik_tahun_id', 'prodi_prodi_id', 'mata_kuliah_mata_kuliah_id', 'pegawai_pegawai_id', 'schedule_schedule_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tahunAkademik()
    {
        return $this->belongsTo('App\Models\TahunAkademik', 'tahun_akademik_tahun_id', 'tahun_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function prodi()
    {
        return $this->belongsTo('App\Models\Prodi', 'prodi_prodi_id', 'prodi_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mataKuliah()
    {
        return $this->belongsTo('App\Models\MataKuliah', 'mata_kuliah_mata_kuliah_id', 'mata_kuliah_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function pegawai()
    {
        return $this->belongsTo('App\Models\Pegawai', 'pegawai_pegawai_id', 'pegawai_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tSchedules()
    {
        return $this->hasMany('App\Models\TSchedule', 'rencana_studi_id_rencana', 'id_rencana');
    }
}

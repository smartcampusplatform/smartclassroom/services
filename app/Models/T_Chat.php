<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $chat_id
 * @property int $chat_chatroom_id
 * @property string $user_username
 * @property string $text_chat
 * @property string $chat_time
 * @property int $root
 * @property Chat $chat
 * @property User $user
 */
class T_Chat extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 't_chat';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'chat_id';
    public $timestamps = false;
    /**
     * @var array
     */
    protected $fillable = ['chat_chatroom_id', 'user_username', 'text_chat', 'chat_time', 'root'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function chat()
    {
        return $this->belongsTo('App\Models\Chat', 'chat_chatroom_id', 'chatroom_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_username', 'username');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $quiz_id
 * @property string $quiz_name
 * @property TAttendance[] $tAttendances
 */
class Quiz extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'quiz';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'quiz_id';

    /**
     * @var array
     */
    protected $fillable = ['quiz_name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tAttendances()
    {
        return $this->hasMany('App\Models\TAttendance', 'quiz_quiz_id', 'quiz_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $inventory_inventoryid
 * @property boolean $inventory_status
 * @property int $inventory_jumlah
 * @property float $image_loc
 * @property int $jumlah_byimage
 * @property string $image_taken
 * @property Inventory $inventory
 */
class T_Inventory extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 't_inventory';

    /**
     * @var array
     */
    protected $fillable = ['inventory_inventoryid', 'inventory_status', 'inventory_jumlah', 'image_loc', 'jumlah_byimage', 'image_taken'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function inventory()
    {
        return $this->belongsTo('App\Models\Inventory', 'inventory_inventoryid', 'inventory_id');
    }
}

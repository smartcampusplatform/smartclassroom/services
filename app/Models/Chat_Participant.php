<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $chat_chatroom_id
 * @property string $user_username
 * @property Chat $chat
 * @property User $user
 */
class Chat_Participant extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'chat_participant';

    /**
     * @var array
     */
    protected $fillable = ['chat_chatroom_id', 'user_username'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function chat()
    {
        return $this->belongsTo('App\Models\Chat', 'chat_chatroom_id', 'chatroom_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_username', 'username');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $pegawai_id
 * @property string $user_username
 * @property string $nip
 * @property string $pegawai_name
 * @property string $pangkat
 * @property string $jabatan
 * @property boolean $is_dosen
 * @property User $user
 * @property RencanaStudi[] $rencanaStudis
 */
class Pegawai extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'pegawai';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'pegawai_id';

    /**
     * @var array
     */
    protected $fillable = ['user_username', 'nip', 'pegawai_name', 'pangkat', 'jabatan', 'is_dosen'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_username', 'username');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rencanaStudis()
    {
        return $this->hasMany('App\Models\RencanaStudi', 'pegawai_pegawai_id', 'pegawai_id');
    }
}

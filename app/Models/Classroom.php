<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $classroom_id
 * @property string $classroom_name
 * @property float $classroom_loc
 * @property int $building_id
 * @property TSchedule[] $tSchedules
 * @property Inventory[] $inventories
 */
class Classroom extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'classroom';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'classroom_id';

    /**
     * @var array
     */
    protected $fillable = ['classroom_name', 'classroom_loc', 'building_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tSchedules()
    {
        return $this->hasMany('App\Models\TSchedule', 'classroom_classroom_id', 'classroom_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function inventories()
    {
        return $this->hasMany('App\Models\Inventory', 'classroom_classroomid', 'classroom_id');
    }
}

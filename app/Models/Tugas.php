<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $tugas_id
 * @property string $tugas_name
 * @property string $tugas_created
 * @property string $tugas_deadline
 * @property TAttendance[] $tAttendances
 */
class Tugas extends Model
{
    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'tugas_id';

    /**
     * @var array
     */
    protected $fillable = ['tugas_name', 'tugas_created', 'tugas_deadline'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tAttendances()
    {
        return $this->hasMany('App\Models\TAttendance', 'tugas_tugas_id', 'tugas_id');
    }
}

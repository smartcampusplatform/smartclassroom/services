<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $notification_id
 * @property integer $notification_type
 * @property string $notification_name
 * @property TNotification[] $tNotifications
 */
class Notification extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'notification';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'notification_id';

    /**
     * @var array
     */
    protected $fillable = ['notification_type', 'notification_name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tNotifications()
    {
        return $this->hasMany('App\Models\TNotification', 'notification_notification_id', 'notification_id');
    }
}

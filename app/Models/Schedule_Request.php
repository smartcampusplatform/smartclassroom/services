<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $request_id
 * @property string $user_username
 * @property boolean $status_request
 * @property string $time_request
 * @property User $user
 * @property TSchedule[] $tSchedules
 */
class Schedule_Request extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'schedule_request';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'request_id';

    /**
     * @var array
     */
    protected $fillable = ['user_username', 'status_request', 'time_request'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_username', 'username');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tSchedules()
    {
        return $this->hasMany('App\Models\TSchedule', 'schedule_request_request_id', 'request_id');
    }
}

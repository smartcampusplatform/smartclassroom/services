<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $tahun_id
 * @property int $tahun
 * @property int $semester
 * @property RencanaStudi[] $rencanaStudis
 */
class Tahun_Akademik extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tahun_akademik';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'tahun_id';

    /**
     * @var array
     */
    protected $fillable = ['tahun', 'semester'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rencanaStudis()
    {
        return $this->hasMany('App\Models\RencanaStudi', 'tahun_akademik_tahun_id', 'tahun_id');
    }
}

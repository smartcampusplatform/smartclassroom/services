<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $t_schedule_id
 * @property string $voice_created
 * @property string $voice_location
 * @property string $ba_generated
 * @property TSchedule $tSchedule
 */
class Berita_Acara extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    public $timestamps = false;
    protected $table = 'berita_acara';

    /**
     * @var array
     */
    protected $fillable = ['t_schedule_id', 'voice_created', 'voice_location', 'ba_generated'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tSchedule()
    {
        return $this->belongsTo('App\Models\TSchedule');
    }
}

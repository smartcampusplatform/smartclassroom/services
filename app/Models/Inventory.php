<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $inventory_id
 * @property int $classroom_classroomid
 * @property string $inventory_name
 * @property string $schedule
 * @property string $last_check
 * @property Classroom $classroom
 * @property TInventory[] $tInventories
 */
class Inventory extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'inventory';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'inventory_id';

    /**
     * @var array
     */
    protected $fillable = ['classroom_classroomid', 'inventory_name', 'schedule', 'last_check'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function classroom()
    {
        return $this->belongsTo('App\Models\Classroom', 'classroom_classroomid', 'classroom_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tInventories()
    {
        return $this->hasMany('App\Models\TInventory', 'inventory_inventoryid', 'inventory_id');
    }
}

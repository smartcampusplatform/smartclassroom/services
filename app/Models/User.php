<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;

/**
 * @property string $username
 * @property string $biometric
 * @property string $email
 * @property string $token
 * @property Mahasiswa[] $mahasiswas
 * @property TNotification[] $tNotifications
 * @property Pegawai[] $pegawais
 * @property MediaSharing[] $mediaSharings
 * @property ScheduleRequest[] $scheduleRequests
 * @property TChat[] $tChats
 * @property ChatParticipant[] $chatParticipants
 */
class User extends Model implements Authenticatable
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'user';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'username';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    use AuthenticableTrait;
    protected $fillable = ['biometric', 'email', 'token'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function mahasiswas()
    {
        return $this->hasMany('App\Models\Mahasiswa', 'user_username', 'username');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tNotifications()
    {
        return $this->hasMany('App\Models\TNotification', 'user_username', 'username');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pegawais()
    {
        return $this->hasMany('App\Models\Pegawai', 'user_username', 'username');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function mediaSharings()
    {
        return $this->hasMany('App\Models\MediaSharing', 'user_username', 'username');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function scheduleRequests()
    {
        return $this->hasMany('App\Models\ScheduleRequest', 'user_username', 'username');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tChats()
    {
        return $this->hasMany('App\Models\TChat', 'user_username', 'username');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function chatParticipants()
    {
        return $this->hasMany('App\Models\ChatParticipant', 'user_username', 'username');
    }
}

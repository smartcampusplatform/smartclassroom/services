<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $chatroom_id
 * @property string $chatroom_admin
 * @property string $chatroom_created
 * @property string $chatroom_modified
 * @property TChat[] $tChats
 * @property ChatParticipant[] $chatParticipants
 */
class Chat extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'chat';
    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'chatroom_id';

    /**
     * @var array
     */
    public $timestamps = false;
    protected $fillable = ['chatroom_admin', 'chatroom_created', 'chatroom_modified'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tChats()
    {
        return $this->hasMany('App\Models\TChat', 'chat_chatroom_id', 'chatroom_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function chatParticipants()
    {
        return $this->hasMany('App\Models\ChatParticipant', 'chat_chatroom_id', 'chatroom_id');
    }
}

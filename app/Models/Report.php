<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $report_id
 * @property integer $report_type
 * @property string $report_title
 * @property string $report_created
 * @property string $report_loc
 */
class Report extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'report';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'report_id';

    /**
     * @var array
     */
    protected $fillable = ['report_type', 'report_title', 'report_created', 'report_loc'];

}

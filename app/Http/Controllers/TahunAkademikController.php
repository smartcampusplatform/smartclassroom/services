<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 13/05/2019
 * Time: 23:38
 */

namespace App\Http\Controllers;


use App\Models\Tahun_Akademik;
use Illuminate\Http\Request;
use Swagger\Annotations as SWG;

class TahunAkademikController extends Controller
{
    public function __construct()
    {

    }

    /**
     *   @SWG\Get(
     *   path="/api/tahunakademik",
     *   operationId="index",
     *   summary="Mendapatkan seluruh tahun akademik ",
     *   tags={"tahun akademik"},
     *   @SWG\Response(
     *     response=200,
     *     description="Working"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error",
     *   )
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $tahun  = Tahun_Akademik::all();
        return response()->json(json_decode($tahun),200,[],JSON_PRETTY_PRINT);
    }

    /**
     * @SWG\Post(
     *   path="/api/tahunakademik",
     *     operationId="store",
     *     tags={"tahun akademik"},
     *     @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="the item to create",
     *     @SWG\Schema(
     *     @SWG\Property(
     *     property="tahun",
     *     type="integer",
     *     example=2020,
     *      ),@SWG\Property(
     *     property="semester",
     *     type="integer",
     *     example=1,
     *      ),
     *      )
     *     ),
     *     @SWG\Response(
     *     response=201,
     *     description="Resource Created"
     *      ),
     *     @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error",
     *      )
     * )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {

        $tahun = Tahun_Akademik::create($request->all());
        return response()->json([
            'message'=>'Resource Added',
            'data'=>json_decode($tahun)
        ],201,[],JSON_PRETTY_PRINT);
    }


}
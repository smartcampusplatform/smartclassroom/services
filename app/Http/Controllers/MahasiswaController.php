<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 13/05/2019
 * Time: 21:55
 */

namespace App\Http\Controllers;


use App\Models\Mahasiswa;
use Illuminate\Http\Request;
use Swagger\Annotations as SWG;

class MahasiswaController extends Controller
{
    public  function __construct()
    {
        $this->middleware('auth');

    }

    /**
     *   @SWG\Get(
     *   path="/api/mahasiswa",
     *   operationId="index",
     *   summary="Get All Student registered in ITB",
     *     description="Token yang diberikan hanya untuk staf akademik/dosen",
     *   tags={"mahasiswa"},
     *   @SWG\Parameter(
     *   name="u",
     *   in="query",
     *   description="Token SSO",
     *   required=true,
     *   type="string"
     *     ),
     *   @SWG\Response(
     *     response=200,
     *     description="Working"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error",
     *   )
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $user= $request->user();
        $role = [0,2,4];
        if(!empty($user)&&(in_array($user->login_role,$role)))
        {
            $mahasiswa_list = Mahasiswa::all()->sortBy('mahasiswa_id');
            return response()->json(json_decode($mahasiswa_list),200,[], JSON_PRETTY_PRINT);
        }
       else
            return response()->json(['message'=>'Only Academic Members Only',
            ],400,[],JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);

    }

    public function store(Request $request)
    {

    }

}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Berita_Acara;
use Illuminate\Support\Facades\Storage;


class ClassroomReportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function addVoice(Request $request){
        $data = new Berita_Acara();
        $data->t_schedule_id = $request->input('t_schedule_id');
        $data->voice_created = $data->freshTimestamp();
        //$voice = $this->voiceRecorder();
        $data->voice_location = Storage::disk('local')->put($data->id.'.txt', 'Test');
        $data->save();

        return response()->json($data,200,[],JSON_PRETTY_PRINT);
    }

    public function voiceRecorder()
    {
//        $voice = new VoiceRecord();
//        return $voice;
    }

    //
}

<?php

namespace App\Http\Controllers;

class NotificationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function sendNotificationClassroomScheduleRequest(Request $request)
    {

    }

    public function sendNotificationReportClassroomUsage(Request $request)
    {

    }

    public function sendNotificationUpdateInventori(Request $request)
    {

    }

    public function sendNotificationVirtualAttendance(Request $request)
    {

    }

    //
}

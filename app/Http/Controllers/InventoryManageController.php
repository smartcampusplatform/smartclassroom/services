<?php

namespace App\Http\Controllers;

use App\Models\Inventory;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class InventoryManageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function createBAUpdateInventori(Request $request)
    {

    }


    public function createMoitoringInventori(Request $request)
    {

    }

    public function getInventory()
    {
        {
            $data = DB::table('t_inventory')
                ->join('inventory','t_inventory.inventory_inventoryid', '=', 'inventory.inventory_id')
                ->join('classroom','inventory.classroom_classroomid', '=', 'classroom.classroom_id')
                ->select('t_inventory.id', 'inventory.inventory_name', 'classroom.classroom_name','t_inventory.inventory_jumlah')->get();
            return response()->json($data,200,[],JSON_PRETTY_PRINT);
        }
    }

    public function show(Request $request, $id){
        $data = DB::table('t_inventory')
            ->join('inventory','t_inventory.inventory_inventoryid', '=', 'inventory.inventory_id')
            ->join('classroom','inventory.classroom_classroomid', '=', 'classroom.classroom_id')
            ->select('t_inventory.id', 'inventory.inventory_name', 'classroom.classroom_name','t_inventory.inventory_jumlah')
            ->where('t_inventory.id','=', $id)->get();
        return response()->json($data,200,[],JSON_PRETTY_PRINT);
    }

    public function updateInventory(Request $request,$id)
    {
        $data = DB::table('t_inventory')
            ->join('inventory','t_inventory.inventory_inventoryid', '=', 'inventory.inventory_id')
            ->join('classroom','inventory.classroom_classroomid', '=', 'classroom.classroom_id')
            ->select('t_inventory.id', 'inventory.inventory_id','inventory.classroom_classroomid','inventory.inventory_name', 'classroom.classroom_name','t_inventory.inventory_jumlah')
            ->where('t_inventory.id','=', $id)->get();
//        $data->inventory_id = $request->input('inventory_name');
        $data->classroom_classroomid = $request->input('classroom_name');
//        $data->inventory_jumlah = $request->input('inventory_jumlah');
        if($data->save())
        {
            return $this->show($id);
            //return response()->json(['status' => 'success', $data]);
        }
        else return response()->json(['status' => 'fail'],401);

    }

    public function verifikasiInventori(Request $request)
    {

    }
    //
}

<?php

namespace App\Http\Controllers;

use App\Models\T_Schedule;
use App\Models\Classroom;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Swagger\Annotations as SWG;

class ClassroomScheduleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//
    }

    /**
     *
     * @SWG\Get(
     *     path="/api/classroom/schedule",
     *     operationId="getClassroomSchedule",
     *     description="mendapatkan jadwal dari seluruh ruangan",
     *     @SWG\Response(
     *     response=200,
     *     description="Working"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   )     *
     * )
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getClassroomSchedule()
    {
        {
            $data = DB::table('t_schedule')
                ->join('classroom','t_schedule.classroom_classroom_id', '=', 'classroom.classroom_id')
                ->join('schedule','t_schedule.schedule_schedule_id', '=', 'schedule.schedule_id')
                ->join('rencana_studi','t_schedule.rencana_studi_id_rencana', '=', 'rencana_studi.id_rencana')
                ->join('mata_kuliah','rencana_studi.mata_kuliah_mata_kuliah_id', '=', 'mata_kuliah.mata_kuliah_id')
//            ->where('t_schedule.id',$id)
                ->select('t_schedule.id', 'classroom.classroom_name',
                    'mata_kuliah.kode_mata_kuliah', 'mata_kuliah.mata_kuliah',
                    'schedule.schedule_date', 'schedule.schedule_time_start')->get();
            return response()->json($data,200,[],JSON_PRETTY_PRINT);
        }

    }
    public function getClassroomScheduleAvailability()
    {
        $data = DB::table('t_schedule')
//            ->join('mahasiswa','t_attendance.mahasiswa_mahasiswa_id', '=', 'mahasiswa.mahasiswa_id')
//            ->join('t_schedule','t_attendance.t_schedule_id', '=', 't_schedule.id')
//            ->join('schedule','t_schedule.schedule_schedule_id', '=', 'schedule.schedule_id')
//            ->join('rencana_studi','t_schedule.rencana_studi_id_rencana', '=', 'rencana_studi.id_rencana')
//            ->join('mata_kuliah','rencana_studi.mata_kuliah_mata_kuliah_id', '=', 'mata_kuliah.mata_kuliah_id')
//            ->select('mata_kuliah.kode_mata_kuliah', 'mata_kuliah.mata_kuliah', 'schedule.schedule_date', 'schedule.schedule_time_start', 'mahasiswa.nim', 'mahasiswa.mahasiswa_name', 't_attendance.attendance')
            ->get();
        return response()->json($data,200,[],JSON_PRETTY_PRINT);

    }
    public function searchClassroom(Request $request)
    {
        $data = DB::table('t_schedule')
//            ->join('mahasiswa','t_attendance.mahasiswa_mahasiswa_id', '=', 'mahasiswa.mahasiswa_id')
//            ->join('t_schedule','t_attendance.t_schedule_id', '=', 't_schedule.id')
//            ->join('schedule','t_schedule.schedule_schedule_id', '=', 'schedule.schedule_id')
//            ->join('rencana_studi','t_schedule.rencana_studi_id_rencana', '=', 'rencana_studi.id_rencana')
//            ->join('mata_kuliah','rencana_studi.mata_kuliah_mata_kuliah_id', '=', 'mata_kuliah.mata_kuliah_id')
//            ->select('mata_kuliah.kode_mata_kuliah', 'mata_kuliah.mata_kuliah', 'schedule.schedule_date', 'schedule.schedule_time_start', 'mahasiswa.nim', 'mahasiswa.mahasiswa_name', 't_attendance.attendance')
            ->get();
        return response()->json($data,200,[],JSON_PRETTY_PRINT);

    }
    public function updateClassroomSchedule(Request $request, $id){
        $data = T_Schedule::where('id',$id)->first();
        $data->schedule_schedule_id = $request->input('schedule');
//        if($data->save())
//        {
//            return $this->show($data->id);
//            //return response()->json(['status' => 'success', $data]);
//        }
//        else
            return response()->json(['status' => 'fail'],401);
    }


    public function create(Request $request)
    {
        

    }

    public function verifikasiClassroomSchedule(Request $request)
    {

    }

    public function getList()
    {
        $data = Classroom::all();
        return response()->json($data,200,[],JSON_PRETTY_PRINT);
    }
    //
}

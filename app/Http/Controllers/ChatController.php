<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\T_Chat;
use App\Models\Chat;

class ChatController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getChatTransaction(){
        $data = T_Chat::all();
        return response()->json($data,200,[],JSON_PRETTY_PRINT);
    }

    public function addTextChat(Request $request){
        $data = new T_Chat();
        $data->chat_chatroom_id = $request->input('chatroom_id');
        $data->text_chat = $request->input('text_chat');
        $data->chat_time = $data->freshTimestamp();
        $data->root = $request->input('root');
        $data->user_username = $request->input('username');
        if($data->save())
        {
            return response()->json($data,200,[],JSON_PRETTY_PRINT);
            //return response()->json(['status' => 'success', $data]);
        }
        else return response()->json(['status' => 'fail'],401);

    }

    public function createChatroom(Request $request){
        $data = new Chat;
        $data->chatroom_admin = $request->input('chatroom_admin');
        $data->chatroom_created = $data->freshTimestamp();
        $data->chatroom_modified = $data->freshTimestamp();
        if($data->save())
        {
            return response()->json($data,200,[],JSON_PRETTY_PRINT);
            //return response()->json(['status' => 'success', $data]);
        }
        else return response()->json(['status' => 'fail'],401);
    }
    //
}

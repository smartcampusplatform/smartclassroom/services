<?php

namespace App\Http\Controllers;

class ReportingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function createBAClassroomUsage(Request $request)
    {

    }

    public function createEvaluationAttendanceVoiceSanitizer(Request $request)
    {

    }

    public function createEvaluationClassroomUsage(Request $request)
    {

    }

    public function createRecapAttendanceVoiceSanitizer(Request $request)
    {

    }

    public function createRecapReportClassroomUsage(Request $request)
    {

    }

    public function createReportAttendanceVoiceSanitizer(Request $request)
    {

    }

    public function createReportClassroomUsage(Request $request)
    {

    }

    public function getBAClassroomUsage(Request $request)
    {

    }

    public function getRecapAttendanceVoiceSanitizer(Request $request)
    {

    }

    public function getRecapReportClassroomUsage(Request $request)
    {

    }

    public function getReportAttendanceVoiceSanitizer(Request $request)
    {

    }

    public function getReportClassroomUsage(Request $request)
    {

    }

    //
}

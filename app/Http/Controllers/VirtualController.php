<?php

namespace App\Http\Controllers;

class VirtualController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    public function addEnrollKey(Request $request)
    {

    }

    public function closeVirtual(Request $request)
    {

    }


    public function getVirtual(Request $request)
    {

    }


    public function getVirtualAttendance(Request $request)
    {

    }


    public function getVirtualSchedule(Request $request)
    {

    }


    public function updateVirtualSchedule(Request $request)
    {

    }

    public function createEvaluationVirtual(Request $request)
    {

    }


    public function verifikasiVirtualAttendance(Request $request)
    {

    }

    //
}

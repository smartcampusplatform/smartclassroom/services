<?php

namespace App\Http\Controllers;

use App\Models\Classroom;
use App\Models\Schedule;
use App\Models\Schedule_Request;
use App\Models\T_Schedule;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Swagger\Annotations as SWG;

class ClassroomRequestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');

    }

    /**
     * @SWG\Post(
     *     path="/api/request",
     *     operationId="addClassroomScheduleRequest",
     *     tags={"schedule request"},
     *     @SWG\Parameter(
     *   name="u",
     *   in="query",
     *   description="Token SSO",
     *   required=true,
     *   type="string"
     *     ),
     *     @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     @SWG\Schema(
     *     @SWG\Property(
     *     property="class_id",
     *     type="integer",
     *     example=1,
     *      ),
     *     @SWG\Property(
     *     property="schedule_date",
     *     type="string",
     *     example="2019-01-21",
     *      ),
     *     @SWG\Property(
     *     property="schedule_time_start",
     *     type="string",
     *     example="13:00:00",
     *      ),
     *     @SWG\Property(
     *     property="schedule_time_end",
     *     type="string",
     *     example="15:00:00",
     *      ),
     *      )
     * ),
     *     @SWG\Response(
     *     response=201,
     *     description="OK"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   )
     * )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addClassroomScheduleRequest(Request $request)
    {

        $class_id= $request->input('class_id');
        $kelas= Classroom::whereClassroomId($class_id)->first();
        $date= $request->input('schedule_date');
        $start= $request->input('schedule_time_start');
        $end= $request->input('schedule_time_end');
        $username= $request->user()->login_id;
        $schedule= new Schedule();
        $schedule->schedule_time_start=$start;
        $schedule->schedule_date= $date;
        $schedule->schedule_time_end= $end;
        if ($schedule->save())
        {
            $schedule_req= new Schedule_Request();
            $schedule_req->user_username=$username;
//            dd($schedule_req);

            if($schedule_req->save())
            {
                $t_schedule = new T_Schedule();
                $t_schedule->schedule_schedule_id= $schedule->schedule_id;
                $t_schedule->classroom_classroom_id=$class_id;
                $t_schedule->schedule_request_request_id= $schedule_req->request_id;
                if($t_schedule->save())
                {
                    return response()->json(['message' => 'Sukses',
                        'data' => [
                            'request_id'=>$schedule_req->request_id,
                            'classroom_name'=>$kelas->classroom_name,
                            'schedule_date'=>$date,
                            'schedule_time_start'=>$start,
                            'schedule_time_end'=>$end,
                            'user_username'=>$username

                        ]
                    ],
                        201,[],JSON_PRETTY_PRINT);

                }

            }
        }



    }
    public function deleteClassroomScheduleRequest(Request $request)
    {

    }

    /**
     * @SWG\Get(
     *   path="/api/request",
     *   operationId="getClassroomScheduleRequest",
     *   summary="Mengambil Data Permintaan Jadwal Ruangan yang tersimpan dalam service SmartClassroom",
     *   tags={"schedule request"},
     *   @SWG\Response(
     *     response=200,
     *     description="Working"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   )
     * )
     */
    public function getClassroomScheduleRequest()
    {
        $data = DB::table('schedule_request')
            ->join('t_schedule','schedule_request.request_id', '=', 't_schedule.schedule_request_request_id')
            ->join('classroom','t_schedule.classroom_classroom_id', '=', 'classroom.classroom_id')
            ->join('schedule','t_schedule.schedule_schedule_id', '=', 'schedule.schedule_id')
//            ->join('rencana_studi','t_schedule.rencana_studi_id_rencana', '=', 'rencana_studi.id_rencana')
//            ->join('mata_kuliah','rencana_studi.mata_kuliah_mata_kuliah_id', '=', 'mata_kuliah.mata_kuliah_id')
//            ->where('t_schedule.id',$id)
            ->select('schedule_request.request_id', 'classroom.classroom_name',
//                'mata_kuliah.kode_mata_kuliah', 'mata_kuliah.mata_kuliah',
                'schedule.schedule_date', 'schedule.schedule_time_start', 'schedule_request.user_username')
//                'berita_acara.ba_generated')
            ->get();
        return response()->json($data,200,[],JSON_PRETTY_PRINT);
//        return response()->json(['status' => 'success', $data]);
    }
    public function printClassroomScheduleRequest(Request $request)
    {

    }
    public function updateClassroomScheduleRequest(Request $request)
    {

    }

    /**
     * @SWG\Put(
     *     path="/api/request/verifikasi/{id}",
     *     operationId="verifikasiClassroomScheduleRequest",
     *     summary="verifikasi permintaan jadwal ruangan kelas",
     *     tags={"schedule request"},
     *     @SWG\Parameter(
     *         name="u",
     *         in="query",
     *         description="Token SSO",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="integer",
     *
     *
     * ),
     *     @SWG\Response(
     *     response=201,
     *     description="Working"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   )
     * )
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function verifikasiClassroomScheduleRequest(Request $request, $id)
    {
        $schedule_req = Schedule_Request::whereRequestId($id)->first();
        $schedule_req->status_request=true;
        if ($schedule_req->save())
        {
            return response()->json(['message'=>'verrified',
                'data'=>$schedule_req],201,[],JSON_PRETTY_PRINT);
        }


    }
    //
}

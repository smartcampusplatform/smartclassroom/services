<?php


namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\T_Attendance;

class AttendanceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getAttendance()
    {
            $data = DB::table('t_attendance')
                ->join('mahasiswa','t_attendance.mahasiswa_mahasiswa_id', '=', 'mahasiswa.mahasiswa_id')
                ->join('t_schedule','t_attendance.t_schedule_id', '=', 't_schedule.id')
                ->join('schedule','t_schedule.schedule_schedule_id', '=', 'schedule.schedule_id')
                ->join('rencana_studi','t_schedule.rencana_studi_id_rencana', '=', 'rencana_studi.id_rencana')
                ->join('mata_kuliah','rencana_studi.mata_kuliah_mata_kuliah_id', '=', 'mata_kuliah.mata_kuliah_id')
                ->select('mata_kuliah.kode_mata_kuliah', 'mata_kuliah.mata_kuliah', 'schedule.schedule_date', 'schedule.schedule_time_start', 'mahasiswa.nim', 'mahasiswa.mahasiswa_name', 't_attendance.attendance')->get();
            return response()->json($data,200,[],JSON_PRETTY_PRINT);

    }

    public function show($id){
        $data = DB::table('t_attendance')
            ->join('mahasiswa','t_attendance.mahasiswa_mahasiswa_id', '=', 'mahasiswa.mahasiswa_id')
            ->join('t_schedule','t_attendance.t_schedule_id', '=', 't_schedule.id')
            ->join('schedule','t_schedule.schedule_schedule_id', '=', 'schedule.schedule_id')
            ->join('rencana_studi','t_schedule.rencana_studi_id_rencana', '=', 'rencana_studi.id_rencana')
            ->join('mata_kuliah','rencana_studi.mata_kuliah_mata_kuliah_id', '=', 'mata_kuliah.mata_kuliah_id')
            ->where('t_attendance.id', '=', $id)
            ->select('mata_kuliah.kode_mata_kuliah', 'mata_kuliah.mata_kuliah', 'schedule.schedule_date', 'schedule.schedule_time_start', 'mahasiswa.nim', 'mahasiswa.mahasiswa_name', 't_attendance.attendance')->get();
        return response()->json($data,200,[],JSON_PRETTY_PRINT);
    }

    public function addBiometricMahasiswa(Request $request, $id){
        $data = User::where('username',$id)->first();
        $data->biometric = $request->input('biometric');
        if($data->save())
        {
            return response()->json(['status' => 'success', $data]);
        }
        else return response()->json(['status' => 'fail'],401);
    }

    public function updateAttendance(Request $request, $id){
        $data = T_Attendance::where('id',$id)->first();
        $data->attendance = $request->input('attendance');
        if($data->save())
        {
            return $this->show($data->id);
            //return response()->json(['status' => 'success', $data]);
        }
        else return response()->json(['status' => 'fail'],401);
    }

    public function store(Request $request){
        $data = new T_Attendance();
        $data->save();

        return response('Berhasil Tambah Data');
    }

    public function destroy($id){
        $data = T_Attendance::where('id',$id)->first();
        $data->delete();

        return response('Berhasil Menghapus Data');
    }
    //
}

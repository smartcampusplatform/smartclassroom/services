<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 13/05/2019
 * Time: 22:32
 */

namespace App\Http\Controllers;


use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function __construct()
    {

    }

    /**
     * @SWG\Post(
     *  path="/api/user/register",
     *  description="deprecated, see identitymanager to get access",
     *  operationId="register",
     * summary="Register User",
     *  tags={"user"},
     *     @SWG\Parameter(
     *     name="User",
     *     in="body",
     *     description="User to create",
     *     @SWG\Schema(
     *      @SWG\Property(
     *      property="name",
     *      example="John Doe",
     *      type="string",
     *      ),
     *      @SWG\Property(
     *      property="email",
     *      example="33216028@students.itb.ac.id",
     *       type="string",
     *      ),
     *     @SWG\Property(
     *      property="password",
     *      example="33216028",
     *      type="string",
     *      ),
     *     @SWG\Property(
     *      property="role",
     *      example=1,
     *      minProperties=0,
     *      maxProperties=8,
     *      type="integer",
     *      )
     *     )
     * ),
     *      @SWG\Response(
     *     response=201,
     *     description="Success"
     *      ),
     *     @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error",
     *      )
     * ))
     *
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $name= $request->input('name');
        $email=$request->input('email');
        $role=$request->input('role');
        $password= Hash::make($request->input('password'));

        $register = User::create([
            'name'=>$name,
            'email'=>$email,
            'password'=>$password,
            'role'=>$role
        ]);

        if($register)
        {
            return response()->json([
                'success'=>true,
                'message'=>'Register Success',
                'data'=>$register
            ],201);
        }
        else{
            return response()->json([
                'success'=>false,
                'message'=>'Register Fail',
                'data'=>''
            ],400);
        }
    }

    /**
     * @SWG\Post(
     *     path="/api/user/login",
     *     description="deprecated, see identitymanager to get access",
     *     operationId="login",
     *     summary="Login User",
     *     tags={"user"},
     *     @SWG\Parameter(
     *     name="User",
     *     in="body",
     *     description="User to create",
     *     @SWG\Schema(
     *     @SWG\Property(
     *     property="email",
     *     example="33216028@students.itb.ac.id",
     *     type="string",
     *      ),
     *     @SWG\Property(
     *     property="password",
     *     example="33216028",
     *     type="string",
     *      ),
     *      )
     * ),
     *     @SWG\Response(
     *     response=200,
     *     description="Success!"
     * ),
     *     @SWG\Response(
     *     response=403,
     *     description="The Username or Password not Valid!"
     * ),
     *
     *
     *
     * )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $email= $request->input('email');
        $password= $request->input('password');

        $user = User::where('email', $email)->first();
        if(Hash::check($password,  optional($user)->password))
        {
            $apiToken = base64_encode(Str::random(40));
            $user->update(
                ['remember_token'=>$apiToken]
            );
            {
                return response()->json([
                    'success'=>true,
                    'message'=>'Login Success',
                    'data'=>[
                        'user'=>$user,
                        'token'=>$apiToken
                    ]
                ],201);
            }
        }
        else{
            return response()->json([
                'success'=>false,
                'message'=>'Login Failed',
                'data'=>''
            ]);
        }

    }


}
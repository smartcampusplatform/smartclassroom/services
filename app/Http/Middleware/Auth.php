<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Factory;
use Illuminate\Http\Request;
use App\Models\User;

class Auth
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function authenticate(Request $request)
    {
//        $this->validate($request, [
//            'token' => 'required'
//        ]);
        $user = User::where('token', $request->header('token'))->first();
        if(empty($request->header('token')))
        {
            return response()->json(['status' => 'token required'],401);
        }
        else if($request->header('token')=== $user->token)
        {
            return response()->json(['status' => 'success'],200);
        }
        else{
            return response()->json(['status' => 'not matched token'],401);
        }
    }

    public function register(Request $request){
        $data = new User();
        $data->username = $request->input('username');
        $data->email = $request->input('email');
        $data->token = base64_encode(str_random(40));
        if($data->save())
        {
            return response()->json(['status' => 'success', $data]);
        }
        else return response()->json(['status' => 'fail'],401);

    }

    //
}

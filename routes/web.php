<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

//Route::get('/attendance[/[{id}]]', 'AttendanceController@getAttendance');
$router->get('/', function () use ($router) {
    return $router->app->path();
});

$router->group(['prefix' => 'api', 'middleware' => 'auth'], function () use ($router) {
  $router->get('attendance/',  ['uses' => 'AttendanceController@getAttendance']);
  $router->get('attendance/{id}',  ['uses' => 'AttendanceController@show']);
  $router->put('attendance/addBiometricMahasiswa/{id}',  ['uses' => 'AttendanceController@addBiometricMahasiswa']);
  $router->put('attendance/updateAttendance/{id}',  ['uses' => 'AttendanceController@updateAttendance']);
  $router->get('chat/',  ['uses' => 'ChatController@getChatTransaction']);
  $router->post('chat/',  ['uses' => 'ChatController@createChatroom']);
  $router->post('chat/addTextChat',  ['uses' => 'ChatController@addTextChat']);
  $router->post('voice/',  ['uses' => 'ClassroomReportController@addVoice']);
});

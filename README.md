# SmartClassroom using Lumen PHP Framework


[![Build Status](https://travis-ci.org/laravel/lumen-framework.svg)](https://travis-ci.org/laravel/lumen-framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/lumen-framework/v/stable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![License](https://poser.pugx.org/laravel/lumen-framework/license.svg)](https://packagist.org/packages/laravel/lumen-framework)


---

* [Language](#language)
* [List of API services](#list-of-api-services)
* [Built With](#built-with)
* [Authors](#authors)
* [License](#license)

---

## Language

This services are written in **PHP**

## List of API Services

### Attendance
API untuk service attendance
* **URL** **Method:**
    --
    getAttendance	`GET`
	```
	/api/attendance/{id}
	```
	addBiometricMahasiswa	`PUT`
	```
	/api/attendance/addBiometricMahasiswa/{id}
	```
	updateAttendance	`PUT`
	```
	/api/attendance/updateAttendance/{id}
	```
    --
* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `[{"kode_mata_kuliah":"kode",
					"mata_kuliah":"matkul",
					"schedule_date":"yyy-mm-dd",
					"schedule_time_start":"hh:mm:ss",
					"nim":"nim",
					"mahasiswa_name":"nama",
					"attendance":true}]`

* **Sample Call:**

### Chat
API untuk service Chat
* **URL** **Method:**
    --
    createChatroom	`POST`
	```
	/api/chat/{id}
	```
	getChatTransaction	`GET`
	```
	/api/chat/{id}
	```
	addTextChat	`POST`
	```
	/api/chat/addTextChat/{id}
	```
    --
* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `[{
        "chat_id": "kode",
        "chat_chatroom_id": "kode",
        "text_chat": "Text",
        "chat_time": "yyy-mm-dd",
        "root": null,
        "user_username": "name"
    }]`

* **Sample Call:**

### Chat
API untuk service ClassroomReport
* **URL** **Method:**
    --
    addVoice	`POST`
	```
	/api/voice/
	```
    --
* **Success Response:**

  * **Code:** 200 <br />
    **Content:** ``

* **Sample Call:**

## Built With

* [Lumen](https://lumen.laravel.com/) - The web framework used

## Authors

* **Ryche Pranita (23218015)** - [ryche](http://178.128.104.74:9000/23218015)
* **Jackson Bobby Romano Daba (23218034)** - [jackson](http://178.128.104.74:9000/23218034)
* **Sigit Ati Kusumo (23218077)** - [sigit](http://178.128.104.74:9000/23218077)

## License

This project is licensed under the MIT License
